#include "std.h"
#include "control_data.h"

/**
 * Zustandsautomat
 */
void std() {
	if (reset) {
		zustand = Z_MENU;
		aktion = A_SHOW_MENU;
	}
	switch (zustand) {
	case Z_MENU:
		if (rotOK) {
			if (changeVolume) {
				zustand = Z_VOLUME;
				aktion = A_VOLUME;
//			} else if (showLeaderboard) {
//				zustand = Z_LEADERBOARD;
//				aktion = A_CHANGE_MODE_LEAD;
			} else if (start) {
				zustand = Z_GAME;
				aktion = A_START;
			}
			rotOK = 0;
		} else {
			if (rotL ^ rotR) {
				if (rotR)
					aktion = A_SEL_NEXT;
				else
					aktion = A_SEL_PREV;
			rotL = rotR = 0;
			} else
				aktion = A_IDLE;
		}
		break;
	case Z_VOLUME:
		if (rotOK) {
			zustand = Z_MENU;
			aktion = A_SHOW_MENU;
			rotOK = 0;
		} else {
			if (rotL ^ rotR) {
				if (rotR)
					aktion = A_VOLUME_UP;
				else
					aktion = A_VOLUME_DOWN;
				rotL = rotR = 0;
			} else
				aktion = A_IDLE;
		}
		break;
	case Z_LEADERBOARD:
		if (rotOK ^ fire) {
			if (rotOK) {
				zustand = Z_MENU;
				aktion = A_SHOW_MENU;
			rotOK = 0;
			} else
				aktion = A_CHANGE_MODE_LEAD;
		} else {
			if (rotL ^ rotR) {
				if (rotR)
					aktion = A_SCROLL_DOWN;
				else
					aktion = A_SCROLL_UP;
				rotL = rotR = 0;
			} else
				aktion = A_IDLE;
		}
		break;
	case Z_GAME:
		rotL = rotR = 0;
		if (rotOK) {
			zustand = Z_MENU;
			aktion = A_SHOW_MENU;
			rotOK = 0;
		} else {
			if (fin) {
				if (isBest) {
					zustand = Z_GAME_ENDE;
					aktion = A_FIN;
				} else {
					zustand = Z_GAME_ENDE;
					aktion = A_FIN;
				}
			} else {
				if (fire)
					aktion = A_FIRE;
				else
					aktion = A_GAME;
			}
		}
		break;
	case Z_GAME_ENDE:
		rotL = rotR = 0;
		if (rotOK) {
			zustand = Z_MENU;
			aktion = A_SHOW_MENU;
			rotOK = 0;
		} else
			aktion = A_IDLE;
		break;
	case Z_GAME_ENDE_BEST:
		if (rotOK ^ fire) {
			if (rotOK) {
				zustand = Z_MENU;
				aktion = A_SCHREIBE_LEAD;
				rotOK = 0;
			} else {
				zustand = Z_BUCHSTABE;
				aktion = A_CHAR;
			}
		} else {
			if (rotL ^ rotR) {
				if (rotR)
					aktion = A_NEXT_NAME_POS;
				else
					aktion = A_PREV_NAME_POS;
				rotL = rotR = 0;
			} else
				aktion = A_IDLE;
		}
		break;
	case Z_BUCHSTABE:
		if (rotOK ^ fire) {
			if (fire) {
				zustand = Z_GAME_ENDE_BEST;
				aktion = A_CHAR_END;
			} else
				aktion = A_CAPS_CHAR;
			rotOK = 0;
		} else {
			if (rotL ^ rotR) {
				if (rotR)
					aktion = A_NEXT_CHAR;
				else
					aktion = A_PREV_CHAR;
				rotL = rotR = 0;
			} else
				aktion = A_IDLE;
		}
		break;
	}
	clear_control();
}
