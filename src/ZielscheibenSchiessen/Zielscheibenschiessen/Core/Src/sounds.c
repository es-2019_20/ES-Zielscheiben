#define USE_HAL_UART_REGISTER_CALLBACKS 1
#include "stm32f4xx_hal.h"
#include "sounds.h"
#include "types.h"

void sendStack(uint8_t which);
void sendStackCmd(uint8_t which, uint8_t command);
void sendStackCmdArg(uint8_t which, uint8_t command, uint16_t argument);
void sendStackCmdArgSplit(uint8_t which, uint8_t command, uint8_t argumentHigh, uint8_t argumentLow);

void uint16ToArray(uint16_t value, uint8_t *array);

uint16_t arrayToUint16(uint8_t *array);

uint16_t calculateCheckSum(uint8_t *buffer);

//void parseStack(uint8_t which);
//bool_t validateStack(uint8_t which);

typedef struct {
	UART_HandleTypeDef *huart;

	//uint32_t _timeOutTimer;
	//uint32_t _timeOutDuration;

	//uint8_t _received[DFPLAYER_RECEIVED_LENGTH];
	uint8_t _sending[DFPLAYER_SEND_LENGTH];

	//uint8_t _receivedIndex;

	uint8_t device;

	//uint8_t _handleType;
	//uint8_t _handleCommand;
	//uint16_t _handleParameter;
	//bool_t _isAvailable;
	//bool_t _isSending;
	//bool_t _isReceiving;
	bool_t _isPlaying;
} DFPLAYER_Handle;

DFPLAYER_Handle handles[2] = {
	{
		//._timeOutDuration = 500,
		._sending = {0x7E, 0xFF, 06, 00, 01, 00, 00, 00, 00, 0xEF},
		//._receivedIndex = 0,
		.device = DFPLAYER_DEVICE_SD,
		//._isAvailable = FALSE,
		//._isSending = FALSE,
		//._isReceiving = FALSE
		._isPlaying = FALSE
	},
	{
		//._timeOutDuration = 500,
		._sending = {0x7E, 0xFF, 06, 00, 01, 00, 00, 00, 00, 0xEF},
		//._receivedIndex = 0,
		.device = DFPLAYER_DEVICE_SD,
		//._isAvailable = FALSE,
		//._isSending = FALSE,
		//._isReceiving = FALSE
		._isPlaying = FALSE
	}
};

inline uint16_t arrayToUint16(uint8_t *array){
  uint16_t value = *array;
  value <<=8;
  value += *(array+1);
  return value;
}

inline void uint16ToArray(uint16_t value, uint8_t *array){
  *array = (uint8_t)(value>>8);
  *(array+1) = (uint8_t)(value);
}

inline uint16_t calculateCheckSum(uint8_t *buffer){
  uint16_t sum = 0;
  for (int i=Stack_Version; i<Stack_CheckSum; i++) {
    sum += buffer[i];
  }
  return -sum;
}

//inline bool_t validateStack(uint8_t which){
//  return calculateCheckSum(handles[which]._received) == arrayToUint16(handles[which]._received+Stack_CheckSum);
//}
//
//void parseStack(uint8_t which){
//  uint8_t handleCommand = *(handles[which]._received + Stack_Command);
//  if (handleCommand == 0x41) { //handle the 0x41 ack feedback as a spcecial case, in case the pollusion of _handleCommand, _handleParameter, and _handleType.
//    handles[which]._isSending = FALSE;
//    return;
//  }
//
//  handles[which]._handleCommand = handleCommand;
//  handles[which]._handleParameter = arrayToUint16(handles[which]._received + Stack_Parameter);
//
//  switch (handles[which]._handleCommand) {
//    case 0x3D:
//      handleMessage(which, DFPlayerPlayFinished, handles[which]._handleParameter);
//      break;
//    case 0x3F:
//      if (handles[which]._handleParameter & 0x01) {
//        handleMessage(which, DFPlayerUSBOnline, handles[which]._handleParameter);
//      }
//      else if (handles[which]._handleParameter & 0x02) {
//        handleMessage(which, DFPlayerCardOnline, handles[which]._handleParameter);
//      }
//      else if (handles[which]._handleParameter & 0x03) {
//        handleMessage(which, DFPlayerCardUSBOnline, handles[which]._handleParameter);
//      }
//      break;
//    case 0x3A:
//      if (handles[which]._handleParameter & 0x01) {
//        handleMessage(which, DFPlayerUSBInserted, handles[which]._handleParameter);
//      }
//      else if (handles[which]._handleParameter & 0x02) {
//        handleMessage(which, DFPlayerCardInserted, handles[which]._handleParameter);
//      }
//      break;
//    case 0x3B:
//      if (handles[which]._handleParameter & 0x01) {
//        handleMessage(which, DFPlayerUSBRemoved, handles[which]._handleParameter);
//      }
//      else if (handles[which]._handleParameter & 0x02) {
//        handleMessage(which, DFPlayerCardRemoved, handles[which]._handleParameter);
//      }
//      break;
//    case 0x40:
//      handleMessage(which, DFPlayerError, handles[which]._handleParameter);
//      break;
//    case 0x3C:
//    case 0x3E:
//    case 0x42:
//    case 0x43:
//    case 0x44:
//    case 0x45:
//    case 0x46:
//    case 0x47:
//    case 0x48:
//    case 0x49:
//    case 0x4B:
//    case 0x4C:
//    case 0x4D:
//    case 0x4E:
//    case 0x4F:
//      handleMessage(which, DFPlayerFeedBack, handles[which]._handleParameter);
//      break;
//    default:
//      handleError(which, WrongStack, 0);
//      break;
//  }
//}

void sendStack(uint8_t which){
//  if (handles[which]._sending[Stack_ACK]) {  //if the ack mode is on wait until the last transmition
//    while (handles[which]._isSending) {
//      if(commit(which)) break;
//    }
//    handles[which]._isReceiving = HAL_UART_Receive_DMA(handles[which].huart, handles[which]._received, DFPLAYER_RECEIVED_LENGTH) == HAL_OK;
//  }
	HAL_UART_Transmit_DMA(handles[which].huart, handles[which]._sending, DFPLAYER_SEND_LENGTH);
  //handles[which]._timeOutTimer = HAL_GetTick();
  //handles[which]._isSending = handles[which]._sending[Stack_ACK];

  //if (!handles[which]._sending[Stack_ACK]) { //if the ack mode is off wait 10 ms after one transmition.
    HAL_Delay(10);
  //}
}

inline void sendStackCmd(uint8_t which, uint8_t command){
  sendStackCmdArg(which, command, 0);
}

inline void sendStackCmdArg(uint8_t which, uint8_t command, uint16_t argument){
  handles[which]._sending[Stack_Command] = command;
  uint16ToArray(argument, handles[which]._sending+Stack_Parameter);
  uint16ToArray(calculateCheckSum(handles[which]._sending), handles[which]._sending+Stack_CheckSum);
  sendStack(which);
}

inline void sendStackCmdArgSplit(uint8_t which, uint8_t command, uint8_t argumentHigh, uint8_t argumentLow){
  uint16_t buffer = argumentHigh;
  buffer <<= 8;
  sendStackCmdArg(which, command, buffer | argumentLow);
}

void enableACK(uint8_t which){
  handles[which]._sending[Stack_ACK] = 0x01;
}

void disableACK(uint8_t which){
  handles[which]._sending[Stack_ACK] = 0x00;
}

//uint8_t readType(uint8_t which){
//  handles[which]._isAvailable = FALSE;
//  return handles[which]._handleType;
//}
//
//uint16_t read(uint8_t which){
//  handles[which]._isAvailable = FALSE;
//  return handles[which]._handleParameter;
//}
//
//uint8_t readCommand(uint8_t which){
//  handles[which]._isAvailable = FALSE;
//  return handles[which]._handleCommand;
//}

//bool_t commit(uint8_t which){
//	while(handles[which]._isReceiving) HAL_Delay(1);
////    while (handles[which]._receivedIndex < 10) {
////	if (handles[which]._receivedIndex == 0) {
////      if (handles[which]._received[Stack_Header] == 0x7E) {
////        handles[which]._receivedIndex ++;
////      }
////    }
////    else{
////      switch (handles[which]._receivedIndex) {
////        case Stack_Version:
////          if (handles[which]._received[handles[which]._receivedIndex] != 0xFF) {
////            return handleError(which, WrongStack, 0);
////          }
////          break;
////        case Stack_Length:
////          if (handles[which]._received[handles[which]._receivedIndex] != 0x06) {
////            return handleError(which, WrongStack, 0);
////          }
////          break;
////        case Stack_End:
////          if (handles[which]._received[handles[which]._receivedIndex] != 0xEF) {
////            return handleError(which, WrongStack, 0);
////          }
////          else{
//            if (validateStack(which)) {
//              handles[which]._receivedIndex = 0;
//              parseStack(which);
//              return handles[which]._isAvailable;
//            }
//            else{
//              return handleError(which, WrongStack, 0);
//            }
////          }
////          break;
////      }
////      handles[which]._receivedIndex++;
////    }
////    }
//    return handleError(which, WrongStack, 0);
//
////  if (handles[which]._isSending && (HAL_GetTick()-handles[which]._timeOutDuration>=handles[which]._timeOutDuration)) {
////    return handleError(which, WrongStack, 0);
////  }
//}

//bool_t waitAvailable(uint8_t which, uint32_t duration){
//	if(handles[which]._isAvailable) return commit(which);
//  uint32_t timer = HAL_GetTick();
//  if (!duration) {
//    duration = handles[which]._timeOutDuration;
//  }
//  while (!commit(which)){
//    if (HAL_GetTick() - timer > duration) {
//      return FALSE;
//    }
//
//  }
//  return TRUE;
//}

//bool_t handleError(uint8_t which, uint8_t type, uint16_t parameter){
//  handleMessage(which, type, parameter);
//  return FALSE;
//}
//
//bool_t handleMessage(uint8_t which, uint8_t type, uint16_t parameter){
//  handles[which]._receivedIndex = 0;
//  handles[which]._handleType = type;
//  handles[which]._handleParameter = parameter;
//  handles[which]._isAvailable = TRUE;
//  handles[which]._isSending = FALSE;
//  return handles[which]._isAvailable;
//}

//void setTimeOut(uint8_t which, uint32_t timeOutDuration){
//  handles[which]._timeOutDuration = timeOutDuration;
//}

bool_t isPlaying(uint8_t which){
	return handles[which]._isPlaying;
}

void* begin(UART_HandleTypeDef *huart, uint8_t which/*, bool_t isACK*/, bool_t doReset){

	switch(which){
	case MUSIC_DF:
		handles[which].huart = huart;
		break;
	case FX_DF:
		handles[which].huart = huart;
		break;
	}
	disableACK(which);
//  if (isACK) {
//    enableACK(which);
//  }
//  else{
//    disableACK(which);
//  }

  if (doReset) {
	  dfplayer_reset(which);
    //waitAvailable(which, 3000);
    HAL_Delay(3000);
  }
  else {
    // assume same state as with reset(): online
    //handles[which]._handleType = DFPlayerCardOnline;
  }

  return /*(readType(which) == DFPlayerCardOnline)
		  || (readType(which) == DFPlayerUSBOnline)
		  || !isACK
		  ? */handles + which/* : 0*/;
}

void next(uint8_t which){
  sendStackCmd(which, 0x01);
  handles[which]._isPlaying = TRUE;
}

void previous(uint8_t which){
  sendStackCmd(which, 0x02);
  handles[which]._isPlaying = TRUE;
}

void play(uint8_t which, int32_t fileNumber){
  sendStackCmdArg(which, 0x03, fileNumber);
  handles[which]._isPlaying = TRUE;
}

void volumeUp(uint8_t which){
  sendStackCmd(which, 0x04);
}

void volumeDown(uint8_t which){
  sendStackCmd(which, 0x05);
}

void volume(uint8_t which, uint8_t volume){
  sendStackCmdArg(which, 0x06, volume);
}

void EQ(uint8_t which, uint8_t eq) {
  sendStackCmdArg(which, 0x07, eq);
}

void loop(uint8_t which, int32_t fileNumber) {
  sendStackCmdArg(which, 0x08, fileNumber);
  handles[which]._isPlaying = TRUE;
}

void outputDevice(uint8_t which, uint8_t device) {
  sendStackCmdArg(which, 0x09, device);
  HAL_Delay(200);
  handles[which]._isPlaying = TRUE;
}

void sleep(uint8_t which){
  sendStackCmd(which, 0x0A);
  notifyFinishedPlaying(&handles[which]);
}

void dfplayer_reset(uint8_t which){
  sendStackCmd(which, 0x0C);
  notifyFinishedPlaying(&handles[which]);
}

void dfplayer_start(uint8_t which){
  sendStackCmd(which, 0x0D);
  handles[which]._isPlaying = TRUE;
}

void pause(uint8_t which){
  sendStackCmd(which, 0x0E);
  notifyFinishedPlaying(&handles[which]);
}

void playFolder(uint8_t which, uint8_t folderNumber, uint8_t fileNumber){
  sendStackCmdArgSplit(which, 0x0F, folderNumber, fileNumber);
  handles[which]._isPlaying = TRUE;
}

void outputSetting(uint8_t which, bool_t enable, uint8_t gain){
  sendStackCmdArgSplit(which, 0x10, enable, gain);
}

void enableLoopAll(uint8_t which){
  sendStackCmdArg(which, 0x11, 0x01);
}

void disableLoopAll(uint8_t which){
  sendStackCmdArg(which, 0x11, 0x00);
}

void playMp3Folder(uint8_t which, int32_t fileNumber){
  sendStackCmdArg(which, 0x12, fileNumber);
  handles[which]._isPlaying = TRUE;
}

void advertise(uint8_t which, int32_t fileNumber){
  sendStackCmdArg(which, 0x13, fileNumber);
}

void playLargeFolder(uint8_t which, uint8_t folderNumber, uint16_t fileNumber){
  sendStackCmdArg(which, 0x14, (((uint16_t)folderNumber) << 12) | fileNumber);
  handles[which]._isPlaying = TRUE;
}

void stopAdvertise(uint8_t which){
  sendStackCmd(which, 0x15);
}

void stop(uint8_t which){
  sendStackCmd(which, 0x16);
  notifyFinishedPlaying(&handles[which]);
}

void loopFolder(uint8_t which, int32_t folderNumber){
  sendStackCmdArg(which, 0x17, folderNumber);
  handles[which]._isPlaying = TRUE;
}

void randomAll(uint8_t which){
  sendStackCmd(which, 0x18);
  handles[which]._isPlaying = TRUE;
}

void enableLoop(uint8_t which){
  sendStackCmdArg(which, 0x19, 0x00);
}

void disableLoop(uint8_t which){
  sendStackCmdArg(which, 0x19, 0x01);
}

void enableDAC(uint8_t which){
  sendStackCmdArg(which, 0x1A, 0x00);
}

void disableDAC(uint8_t which){
  sendStackCmdArg(which, 0x1A, 0x01);
}

//int readState(uint8_t which){
//  sendStackCmd(which, 0x42);
//  if (waitAvailable(which, 0)) {
//    if (readType(which) == DFPlayerFeedBack) {
//      return read(which);
//    }
//    else{
//      return -1;
//    }
//  }
//  else{
//    return -1;
//  }
//}
//
//int readVolume(uint8_t which){
//  sendStackCmd(which, 0x43);
//  if (waitAvailable(which, 0)) {
//	  if (readType(which) == DFPlayerFeedBack) {
//		  return read(which);
//	  }
//	  else{
//	        return -1;
//	      }
//  }
//  else{
//    return -1;
//  }
//}
//
//int readEQ(uint8_t which){
//  sendStackCmd(which, 0x44);
//  if (waitAvailable(which, 0)) {
//    if (readType(which) == DFPlayerFeedBack) {
//      return read(which);
//    }
//    else{
//      return -1;
//    }
//  }
//  else{
//    return -1;
//  }
//}
//
//int readFileCountsFromDevice(uint8_t which, uint8_t device){
//  switch (handles[which].device) {
//    case DFPLAYER_DEVICE_U_DISK:
//      sendStackCmd(which, 0x47);
//      break;
//    case DFPLAYER_DEVICE_SD:
//      sendStackCmd(which, 0x48);
//      break;
//    case DFPLAYER_DEVICE_FLASH:
//      sendStackCmd(which, 0x49);
//      break;
//    default:
//      break;
//  }
//
//  if (waitAvailable(which, 0)) {
//    if (readType(which) == DFPlayerFeedBack) {
//      return read(which);
//    }
//    else{
//      return -1;
//    }
//  }
//  else{
//    return -1;
//  }
//}
//
//int readCurrentFileNumberFromDevice(uint8_t which, uint8_t device){
//  switch (handles[which].device) {
//    case DFPLAYER_DEVICE_U_DISK:
//      sendStackCmd(which, 0x4B);
//      break;
//    case DFPLAYER_DEVICE_SD:
//      sendStackCmd(which, 0x4C);
//      break;
//    case DFPLAYER_DEVICE_FLASH:
//      sendStackCmd(which, 0x4D);
//      break;
//    default:
//      break;
//  }
//  if (waitAvailable(which, 0)) {
//    if (readType(which) == DFPlayerFeedBack) {
//      return read(which);
//    }
//    else{
//      return -1;
//    }
//  }
//  else{
//    return -1;
//  }
//}
//
//int readFileCountsInFolder(uint8_t which, int32_t folderNumber){
//  sendStackCmdArg(which, 0x4E, folderNumber);
//  if (waitAvailable(which, 0)) {
//    if (readType(which) == DFPlayerFeedBack) {
//      return read(which);
//    }
//    else{
//      return -1;
//    }
//  }
//  else{
//    return -1;
//  }
//}
//
//int readFolderCounts(uint8_t which){
//  sendStackCmd(which, 0x4F);
//  if (waitAvailable(which, 0)) {
//    if (readType(which) == DFPlayerFeedBack) {
//      return read(which);
//    }
//    else{
//      return -1;
//    }
//  }
//  else{
//    return -1;
//  }
//}
//
//int readFileCounts(uint8_t which){
//  return readFileCountsFromDevice(which, DFPLAYER_DEVICE_SD);
//}
//
//int readCurrentFileNumber(uint8_t which){
//  return readCurrentFileNumberFromDevice(which, DFPLAYER_DEVICE_SD);
//}

//bool_t isAvailable(uint8_t which){
//	return handles[which]._isAvailable;
//}

//void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart){
//	huart->gState = HAL_UART_STATE_READY;
//}
//
//void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
//	int i = 2;
//i += 5;
//	(huart == handles[MUSIC_DF].huart ? handles + MUSIC_DF : handles + FX_DF)->_isReceiving = FALSE;
//	return;
//}

//void data_received(UART_HandleTypeDef *huart){
//	(huart == handles[MUSIC_DF].huart ? handles + MUSIC_DF : handles + FX_DF)->_isReceiving = FALSE;
//	return;
//}

void notifyFinishedPlaying(void * df_handle){
	(df_handle == handles + MUSIC_DF ? handles + MUSIC_DF : handles + FX_DF)->_isPlaying = FALSE;
}
