#include "data.h"

/*
 * aus Zeit, Treffer und wie viele Ziele getroffen wurden
 * wird der Score berechnet und zurückgegeben je nach Modi
 */
uint32_t getScore_mode0(uint32_t zeit, uint8_t treffer[], size_t targetsCnt);
uint32_t getScore_mode1(uint32_t zeit, uint8_t treffer[], size_t targetsCnt);
uint32_t getScore_mode2(uint32_t zeit, uint8_t treffer[], size_t targetsCnt);

/**
 * Die Menüpunkte:
 * 1.Gamemodus TTA(Take them All):
 * 		Laserpistole: Schuss dauert max 10 Sekunden
 * 					  Schussanzahl - unendlich
 * 		Ziele:		  Alle sind zum Start oben
 * 					  Bei Treffer bleibt das Ziel unten
 * Spielendebedingung:Alle Ziele einmal getroffen
 *
 * 2.Gamemodus FatW(Fire at Will):
 * 		Laserpistole: Schuss dauert max 3 Sekunden
 * 					  Schussanzahl - unendlich
 * 		Ziele:		  Alle sind zum Start oben
 * 					  Wenn alle Ziele unten sind und noch Zeit übrig ist,
 * 					  klappen die Ziele wieder hoch
 * Spielendebedingung:Spielzeit abgelaufen
 *
 * 3.Gamemodus 1@aTime( One at a time):
 *  	Laserpistole: Schuss dauert max 3 Sekunden
 * 					  Schussanzahl - 15
 * 		Ziele:		  Nur ein Ziel klappt hoch, bei Treffer klappt ein anderes Ziel
 * 					  hoch bis alle 15 Schuss verbraucht sind
 * Spielendebedingung: alle Schuesse verbraucht
 * 4.Scores:
 * 		Menüpunkt, um die Lautstärke zu ändern
 * 5.Leaderboard:
 * 		Menüpunkt, um die Scorelisten sich anzusehen
 */
menu_item_t menuPunkte[] =
{
	{
		.gamemode =
		{
			.gamemode_idx = 0,
			.laserConf =
			{
				.maxTime = 3000,
				.maxShots = 0
			},
			.servoConf =
			{
				.nextServoUpConf = NONE,
				.minServoDownTime = 0,
				.maxServoDownTime = 0
			},
			.maxTreffer = TARGET_COUNT,
			.maxTime = 0,
			.moreIsBetter = FALSE,
			.getScore = &getScore_mode0
		},
		.menuItemType = GAMEMODE,
		.titel = "TTA"
	},
	{
		.gamemode =
		{
			.gamemode_idx = 1,
			.laserConf =
			{
				.maxTime = 3000,
				.maxShots = 0
			},
			.servoConf =
			{
				.nextServoUpConf = ALL,
				.minServoDownTime = 0,
				.maxServoDownTime = 0
			},
			.maxTreffer = 0,
			.maxTime = 60000,
			.moreIsBetter = TRUE,
			.getScore = &getScore_mode1
		},
		.menuItemType = GAMEMODE,
		.titel = "FatW"
	},
	{
		.gamemode =
		{
			.gamemode_idx = 2,
			.laserConf =
			{
				.maxTime = 3000,
				.maxShots = 15
			},
			.servoConf =
			{
				.nextServoUpConf = ONE_UP_RAND,
				.minServoDownTime = 0,
				.maxServoDownTime = 0
			},
			.maxTreffer = 15,
			.maxTime = 0,
			.moreIsBetter = TRUE,
			.getScore = &getScore_mode2
		},
		.menuItemType = GAMEMODE,
		.titel = "1@aTime"
	},
	{ .menuItemType = LEADERBOARD, .titel = "Scores" },
	{ .menuItemType = VOLUME, .titel = "Volume" }
};

akt_menu_item_t aktAuswahl = { .index = 0, .menuItem = menuPunkte };

uint8_t lautstaerke = VOLUME_INIT_VAL;

display_buffer_t displayPuffer = { .changed = FALSE };

char name[NAME_LENGTH + 1] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

/**
 * Berechnet den Score für den Gamemodus TTA(Take them All)
 * Zeit für den Score relevant
 */
uint32_t getScore_mode0(uint32_t zeit, uint8_t treffer[], size_t targetsCnt){
	return zeit / 10;
}
/**
 * Berechnet den Score für den Gamemodus FatW(Fire at Will)
 * Zeit, getroffene Ziele für den Score relevant
 */
uint32_t getScore_mode1(uint32_t zeit, uint8_t treffer[], size_t targetsCnt){
	static const uint32_t punkte[5] = {100,100,100,200,200};
	uint32_t score = 0;
	for(int i = 0; i < targetsCnt; i++) score += treffer[i] * punkte[i];
	return score;
}
/**
 * Berechnet den Score für den Gamemodus 1@aTime( One at a time)
 * Zeit, getroffene Ziele für den Score relevant
 */
uint32_t getScore_mode2(uint32_t zeit, uint8_t treffer[], size_t targetsCnt){
	static const uint32_t punkte[5] = {100,100,100,200,200};
	uint32_t score = 0;
	for(int i = 0; i < TARGET_COUNT; i++) score += treffer[i] * punkte[i] * 100;
	score /= aktZeit;
	return score;
}
