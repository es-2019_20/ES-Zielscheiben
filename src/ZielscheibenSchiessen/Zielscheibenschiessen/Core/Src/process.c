#include "process.h"
#include "data.h"
#include "control_data.h"
#include "hall_of_fame.h"
#include "sounds.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "ssd1306.h"
#include "fonts.h"

ADC_HandleTypeDef *local_hadc1;

TIM_HandleTypeDef *local_rotenc_timer_handle;
TIM_HandleTypeDef *local_htim3;
TIM_HandleTypeDef *local_htim4;

UART_HandleTypeDef *music_uart_handle;
UART_HandleTypeDef *fx_uart_handle;

/**
 * Initialisiert die Prozesse, die Hardwarezugriff benötigen
 * Alle anderen Prozesse sind stehts Initialisiert
 * Vor dem ersten Aufruf der Prozessaktivierungstabelle aufrufen
 */
void init(ADC_HandleTypeDef *_hadc1, TIM_HandleTypeDef *_rotenc_timer_handle, TIM_HandleTypeDef *_htim3, TIM_HandleTypeDef *_htim4, I2C_HandleTypeDef *_hi2c1, UART_HandleTypeDef *_music_uart_handle, UART_HandleTypeDef *_fx_uart_handle){
	local_hadc1 = _hadc1;
	local_rotenc_timer_handle = _rotenc_timer_handle;
	local_htim3 = _htim3;
	local_htim4 = _htim4;
	music_uart_handle = _music_uart_handle;
	fx_uart_handle = _fx_uart_handle;
	begin(_music_uart_handle, 	MUSIC_DF, TRUE);
	begin(_fx_uart_handle, 	FX_DF, TRUE);
	volume(MUSIC_DF, lautstaerke);
	volume(FX_DF, 30);
	outputDevice(MUSIC_DF, DFPLAYER_DEVICE_SD);
	outputDevice(FX_DF, DFPLAYER_DEVICE_SD);
	playFolder(MUSIC_DF, 1, 1);
	HAL_Delay(1000);
	enableLoop(MUSIC_DF);
	HAL_TIM_Encoder_Start_IT(local_rotenc_timer_handle, TIM_ENCODERMODE_TI12);
	local_rotenc_timer_handle->Instance->CNT = 65535 / 2;
	initHOF();
	ranglistenArray = HOF_getCurDataPos();
	SSD1306_Init();
}

/**
 * Berechnet das Schwellenwert
 * Liest: umgebungsHelligkeit
 * Schreibt: schwellenWert
 */
void schwellenWertBerechnen(void){
	schwellenWert = 0;
	uint8_t cnt = 0;
	for(int i = 0; i < ENVIRONMENT_SENSORS_COUNT; i++)
		for(int j = 0; j < THRESHHOLD_RING_COUNT; j++){
			schwellenWert += umgebungsHelligkeit[i][j];
			if(umgebungsHelligkeit[i][j]) cnt++;
		}
	schwellenWert /= cnt;
	schwellenWert = schwellenWert * 1.3;
}

/**
 * Liest Helligkeiten
 * Liest: hellwertzielX, umhellwertX
 * Schreibt: umgebungsHelligkeit, zielHelligkeit
 */
void leseHelligkeit(void){
	static uint8_t ring_idx = 0;

	HAL_ADC_Start(local_hadc1);

	//Umgebung
	for(int i = 0; i < ENVIRONMENT_SENSORS_COUNT; i++){
		HAL_ADC_PollForConversion(local_hadc1, 100);
		umgebungsHelligkeit[i][ring_idx] = HAL_ADC_GetValue(local_hadc1);
	}

	//Zielscheiben
	for(int i = 0; i < TARGET_SENSORS_COUNT; i++){
		HAL_ADC_PollForConversion(local_hadc1, 100);
		zielHelligkeit[i] = HAL_ADC_GetValue(local_hadc1);
	}
	HAL_ADC_Stop(local_hadc1);
	ring_idx++;
	ring_idx %= THRESHHOLD_RING_COUNT;
}

/**
 * Entscheidet über Treffer
 * Liest: zielHelligkeit, schwellenWert, lastUp
 * Schreibt: lastUp, treffer
 */
void entscheideTreffer(void){
	for(int i = 0; i < TARGET_SENSORS_COUNT; i++){
		if(lastUp[i])
			lastUp[i] = zielHelligkeit[i] >= schwellenWert;
		else if(zielHelligkeit[i] >= schwellenWert)
			treffer[i] += lastUp[i] = 1;
	}
}

/**
 * Hauptlogik eines Spiels
 * Berechnet die Time Delta zwischen Durchläufen
 * Prüft die Spielendebedingungen
 * Berechnet die Punkte
 * Prüft auf Bestenlisteneintrag
 * Zählt Scheiben die Oben sind
 * Klappt Scheiben unter bestimmten Bedingungen hoch
 * Reagiert auf Treffer
 * Liest: startTime, treffer, alteTreffer, laserInfo, servoDown, lastTimeStamp, trefferSum, oneUpAtATimeLastUp, aktZeit, aktAuswahl
 * Schreibt: alteTreffer, servoDown, lastTimeStamp, trefferSum, oneUpAtATimeLastUp, aktZeit, score, fin, isBest, soundauswahl, fxauswahl
 */
void game(void){
	//Hilfsvariable für FX
	static bool_t wasHit = FALSE;
	const uint32_t curTime = HAL_GetTick();
	uint32_t timePassed = 0;
	if(lastTimeStamp == 0){
		timePassed = curTime - startTime;
	}else{
		timePassed = curTime - lastTimeStamp;
	}
	lastTimeStamp = curTime;
	if(isOnceFired)
		aktZeit += timePassed;
	else if(laserInfo.isAn) isOnceFired = TRUE;
	laserInfo.timeStamp += timePassed;

	//Treffersound ende Handling
	if(wasHit && !isPlaying(FX_DF) && laserInfo.isAn){
		playFolder(FX_DF, 1, 1);
		wasHit = FALSE;
	} else if(wasHit && !isPlaying(FX_DF)) wasHit = FALSE;

	bool_t maxTimeReached = aktAuswahl.menuItem->gamemode.maxTime
			&& aktZeit >= aktAuswahl.menuItem->gamemode.maxTime;
	bool_t maxTrefferReached = aktAuswahl.menuItem->gamemode.maxTreffer
			&& trefferSum >= aktAuswahl.menuItem->gamemode.maxTreffer;
	bool_t maxShotsReached = aktAuswahl.menuItem->gamemode.laserConf.maxShots
			&& laserInfo.shotsCounter >= aktAuswahl.menuItem->gamemode.laserConf.maxShots
			&& !laserInfo.isAn;

	//Scoreberechnung
	score = aktAuswahl.menuItem->gamemode.getScore(aktZeit, treffer, TARGET_COUNT);

	//Handle game end
	if(maxTimeReached || maxTrefferReached || maxShotsReached){
		if(ranglistenArray->rd[aktAuswahl.index].data_cnt < GAME_DATEN_HISTORY_LENGTH){
			fin = TRUE;
			isBest = TRUE;
			//advertise(MUSIC_DF, 1);
		}
		else if(aktAuswahl.menuItem->gamemode.moreIsBetter){
			if(ranglistenArray->rd[aktAuswahl.index].daten[9].measure < score){
				fin = TRUE;
				isBest = TRUE;
				//advertise(MUSIC_DF, 1);
			} else {
				fin = TRUE;
				isBest = FALSE;
				//advertise(MUSIC_DF, 2);
			}
		}
		else {
			if(ranglistenArray->rd[aktAuswahl.index].daten[9].measure > score){
				fin = TRUE;
				isBest = TRUE;
				//advertise(MUSIC_DF, 1);
			} else {
				fin = TRUE;
				isBest = FALSE;
				//advertise(MUSIC_DF, 2);
			}
		}
		return;
	}

	//Count targets up
	uint8_t targetsUpCount = 0;
	for(int i = 0; i < TARGET_COUNT; i++)
		targetsUpCount += !servoDown[i];

	//Handle no targets up
	if(!targetsUpCount)
		switch(aktAuswahl.menuItem->gamemode.servoConf.nextServoUpConf){
		size_t r;
			case ALL:
				for(int i = 0; i < TARGET_SERVOS_COUNT; i++)
					servoDown[i] = 0;
				break;
			case ONE_UP_RAND:
				r = rand() % TARGET_COUNT - 1;
				servoDown[ r > oneUpAtATimeLastUp ? r + 1 : r ] = 0;
			default:
				break;
		}

	//Check and Handle hit
	for(int i = 0; i < TARGET_COUNT; i++){
		if(alteTreffer[i]^treffer[i] && !servoDown[i]){
			switch(aktAuswahl.menuItem->gamemode.servoConf.nextServoUpConf){
			case ONE_UP_RAND:
				oneUpAtATimeLastUp = i;
			case NONE:
			case ALL:
				servoDown[i] = -1;
				break;
			case RAND:
				servoDown[i] = aktAuswahl.menuItem->gamemode.servoConf.minServoDownTime + (rand() % (aktAuswahl.menuItem->gamemode.servoConf.maxServoDownTime - aktAuswahl.menuItem->gamemode.servoConf.minServoDownTime));
				break;
			case FIFO:
				servoDown[i] = aktAuswahl.menuItem->gamemode.servoConf.minServoDownTime;
				break;
			}
			trefferSum++;
			alteTreffer[i] = treffer[i];
			playFolder(FX_DF, 1, 2);
			wasHit = TRUE;
		}
	}

	//Hochklappen der Scheiben nach der bestimmten Zeit
	for(int i = 0; i < TARGET_COUNT; i++){
		if(!servoDown[i]) return;
		servoDown[i] = servoDown[i] > 0 && servoDown[i] - timePassed < 0 ? 0 : servoDown[i] - timePassed;
	}
}

/**
 * Initialisiert das Spiel
 * Bringt Scheiben in die Initialposition
 * Setzt Treffer, Score, Laserdaten und timestamps zurück
 * Liest: aktAuswahl
 * Schreibt: alteTreffer, treffer, servoDown, lastTimeStamp, trefferSum, oneUpAtATimeLastUp, aktZeit, score
 */
void startGame(void){

	//Servos initialisieren
	uint8_t which;
	switch(aktAuswahl.menuItem->gamemode.servoConf.nextServoUpConf){
	case NONE:
	case RAND:
	case FIFO:
	case ALL:
		for(int i = 0; i < TARGET_SERVOS_COUNT; i++){
			servoDown[i] = 0;
		}
		break;
	case ONE_UP_RAND:
		which = rand() % TARGET_COUNT;
		for(int i = 0; i < TARGET_SERVOS_COUNT; i++)
			servoDown[i] = which == i ? 0 : -1;
		break;
	}

	//setze alteTreffer und treffer auf 0
	for(int i = 0; i < TARGET_COUNT; i++){
		alteTreffer[i] = treffer[i] = 0;
	}

	//Score, Zeit und Counter auf 0
	score = 0;
	aktZeit = 0;
	laserInfo.shotsCounter = 0;
	trefferSum = 0;

	//StartZeit
	startTime = HAL_GetTick();
	lastTimeStamp = 0;

	isOnceFired = 0;
}


/**
 * Bewegt Servos
 * Liest: servoDown
 * Schreibt: winkelservoX
 */
void schreibeServoWinkel(void){
	//Servo1
	if(servoDown[0]){
		HAL_TIM_PWM_Start(local_htim4, TIM_CHANNEL_1);
		local_htim4->Instance->CCR1 = 145;

	}else {
		HAL_TIM_PWM_Start(local_htim4, TIM_CHANNEL_1);
		local_htim4->Instance->CCR1 = 84;
	}
	//Servo2
	if(servoDown[1]){
		HAL_TIM_PWM_Start(local_htim3, TIM_CHANNEL_1);
		local_htim3->Instance->CCR1 = 84;

	}else{
		HAL_TIM_PWM_Start(local_htim3, TIM_CHANNEL_1);
		local_htim3->Instance->CCR1 = 145;
	}
	//Servo3
	if(servoDown[2]){
		HAL_TIM_PWM_Start(local_htim3, TIM_CHANNEL_2);
		local_htim3->Instance->CCR2 = 84;

	}else{
		HAL_TIM_PWM_Start(local_htim3, TIM_CHANNEL_2);
		local_htim3->Instance->CCR2 = 145;
	}
	//Servo4
	if(servoDown[3]){
		HAL_TIM_PWM_Start(local_htim3, TIM_CHANNEL_3);
		local_htim3->Instance->CCR3 = 145;

	}else{
		HAL_TIM_PWM_Start(local_htim3, TIM_CHANNEL_3);
		local_htim3->Instance->CCR3 = 84;
	}
	//Servo5
	if(servoDown[4]){
		HAL_TIM_PWM_Start(local_htim3, TIM_CHANNEL_4);
		local_htim3->Instance->CCR4 = 145;

	}else{
		HAL_TIM_PWM_Start(local_htim3, TIM_CHANNEL_4);
		local_htim3->Instance->CCR4 = 84;
	}


}

/**
 * Erhöht die Lautstärke
 * Liest: lautstaerke
 * Schreibt: lautstaerke
 */
void lautstaerkeUp(void){
	lautstaerke += lautstaerke < VOLUME_MAX ? 1 : 0;
}

/**
 * Verkleinert die Lautstärke
 * Liest: lautstaerke
 * Schreibt: lautstaerke
 */
void lautstaerkeDown(void){
	lautstaerke -= lautstaerke > VOLUME_MIN ? 1 : 0;
}

/**
 * Schaltet den Laser an wenn der aktuelle Zustand es zulässt
 * Liest: aktAuswahl, laserInfo
 * Schreibt: laserInfo, laser
 */
void laserAn(void){
	if(laserInfo.isAn && laserInfo.timeStamp >= aktAuswahl.menuItem->gamemode.laserConf.maxTime)
		HAL_GPIO_WritePin(GPIOC, Laser_Pin, GPIO_PIN_RESET);
	else if(!laserInfo.isAn
			&& (!aktAuswahl.menuItem->gamemode.laserConf.maxShots
					|| laserInfo.shotsCounter < aktAuswahl.menuItem->gamemode.laserConf.maxShots)){
		HAL_GPIO_WritePin(GPIOC, Laser_Pin, GPIO_PIN_SET);
		laserInfo.timeStamp = 0;
		laserInfo.isAn = TRUE;
		laserInfo.shotsCounter++;
	}
}

/**
 * Schaltet den Laser aus
 * Liest: aktAuswahl, laserInfo
 * Schreibt: laserInfo, laser
 */
void laserAus(void){
	laserInfo.isAn = FALSE;
	HAL_GPIO_WritePin(GPIOC, Laser_Pin, GPIO_PIN_RESET);
}

/**
 * Überträgt die Lautstärke auf das Audiomodul
 * Liest: lautstaerke
 * Schreibt: lautstaerkeModul
 */
void soundAn(void){
	volume(MUSIC_DF, lautstaerke);
}

/**
 * Setzt Kontrollflüsse je nach aktueller Menüauswahl
 * Liest: aktAuswahl
 * Schreibt: start, showLeaderboard, changeVolume
 */
void doAction(void){
	switch(aktAuswahl.menuItem->menuItemType){
	case LEADERBOARD:
		start = FALSE;
		changeVolume = FALSE;
		showLeaderboard = TRUE;
		break;
	case VOLUME:
		start = FALSE;
		changeVolume = TRUE;
		showLeaderboard = FALSE;
		break;
	case GAMEMODE:
		start = TRUE;
		changeVolume = FALSE;
		showLeaderboard = FALSE;
		break;
	}
	strcpy(displayPuffer.texts[0].str, aktAuswahl.menuItem->titel);
	displayPuffer.texts[0].Font = &Font_16x26;
	displayPuffer.texts[0].x = 10;
	displayPuffer.texts[0].y = 20;
	displayPuffer.lines[0].startX = 0;
	displayPuffer.lines[0].startY = 18;
	displayPuffer.lines[0].endX = 128;
	displayPuffer.lines[0].endY = 18;
	displayPuffer.lines[1].startX = 0;
	displayPuffer.lines[1].startY = 48;
	displayPuffer.lines[1].endX = 128;
	displayPuffer.lines[1].endY = 48;
	displayPuffer.texts_cnt = 1;
	displayPuffer.lines_cnt = 2;
	displayPuffer.changed = TRUE;
}

/**
 * Wechselt zum nächsten Menüpunkt
 * Liest: menuPunkte, aktAuswahl
 * Schreibt: aktAuswahl, displayPuffer
 */
void nextMenuPunkt(void){
	aktAuswahl.index++;
	if(aktAuswahl.index>(MODES_CNT + 1)){
		aktAuswahl.index = 0;	
	}
	aktAuswahl.menuItem = &menuPunkte[aktAuswahl.index];
}

/**
 * Wechselt zum letzten Menüpunkt
 * Liest: menuPunkte, aktAuswahl
 * Schreibt: aktAuswahl, displayPuffer
 */
void lastMenuPunkt(void){
	aktAuswahl.index--;
	if(aktAuswahl.index<0){
		aktAuswahl.index = (MODES_CNT + 1);
	}
	aktAuswahl.menuItem = &menuPunkte[aktAuswahl.index];
}

/**
 * Bereitet Daten zur Anzeige auf dem Display
 * Liest: aktZeit, score, aktAuswahl
 * Schreibt: displayPuffer
 */
void gameAnzeige(void){
	const uint32_t min = (aktZeit / 60000) % 10;
	const uint32_t sec = (aktZeit / 1000) % 60;
	const uint32_t ms = (aktZeit / 100) % 10;
	size_t titLen = sprintf(displayPuffer.texts[0].str, "%s", aktAuswahl.menuItem->titel);
	size_t aktZeitLen = sprintf(displayPuffer.texts[1].str, "Time=%lu:%2lu:%lu", min, sec, ms);
	size_t aktScoreLen = sprintf(displayPuffer.texts[2].str, "Score:%5lu", score % 100000);
	displayPuffer.texts[0].Font = &Font_16x26;
	displayPuffer.texts[0].x = (OLED_WIDTH / 2) - (titLen % 2 ? (OLED_CHAR_WIDTH_BIG_FONT / 2) : 0) - OLED_CHAR_WIDTH_BIG_FONT * (titLen / 2);
	displayPuffer.texts[0].y = 0;
	displayPuffer.texts[1].Font = &Font_11x18;
	displayPuffer.texts[1].x = (OLED_WIDTH / 2) - (aktZeitLen % 2 ? (OLED_CHAR_WIDTH_NORMAL_FONT / 2) : 0) - OLED_CHAR_WIDTH_NORMAL_FONT * (aktZeitLen / 2);
	displayPuffer.texts[1].y = OLED_CHAR_HEIGHT_BIG_FONT + 1;
	displayPuffer.texts[2].Font = &Font_11x18;
	displayPuffer.texts[2].x = (OLED_WIDTH / 2) - (aktScoreLen % 2 ? (OLED_CHAR_WIDTH_NORMAL_FONT / 2) : 0) - OLED_CHAR_WIDTH_NORMAL_FONT * (aktScoreLen / 2);
	displayPuffer.texts[2].y = OLED_CHAR_HEIGHT_BIG_FONT + OLED_CHAR_HEIGHT_NORMAL_FONT;
	displayPuffer.texts_cnt = 3;
	displayPuffer.changed = TRUE;
}

/**
 * Bereitet Daten zur Anzeige auf dem Display
 * Liest: lautstaerke
 * Schreibt: displayPuffer
 */
void volumeAnzeige(void){
	strcpy(displayPuffer.texts[0].str, "VOLUME");
	size_t lautLen = sprintf(displayPuffer.texts[1].str, "%d/30", lautstaerke);
	displayPuffer.texts[0].Font = &Font_16x26;
	displayPuffer.texts[0].x = (OLED_WIDTH / 2) - (6 % 2 ? (OLED_CHAR_WIDTH_BIG_FONT / 2) : 0) - OLED_CHAR_WIDTH_BIG_FONT * (6 / 2);
	displayPuffer.texts[0].y = (OLED_HEIGHT - OLED_CHAR_HEIGHT_BIG_FONT * 2) / 3;
	displayPuffer.texts[1].Font = &Font_16x26;
	displayPuffer.texts[1].x = (OLED_WIDTH / 2) - (lautLen % 2 ? (OLED_CHAR_WIDTH_BIG_FONT / 2) : 0) - OLED_CHAR_WIDTH_BIG_FONT * (lautLen / 2);
	displayPuffer.texts[1].y = ((OLED_HEIGHT - OLED_CHAR_HEIGHT_BIG_FONT * 2) / 3) * 2 + OLED_CHAR_HEIGHT_BIG_FONT;
	displayPuffer.texts_cnt = 2;
	displayPuffer.changed = TRUE;
}

/**
 * Berechnet das Schwellenwert
 * Liest: umgebungsHelligkeit
 * Schreibt: schwellenWert
 */
void displayRefresh(void){
	//lines Zeichnen
	SSD1306_Fill(SSD1306_COLOR_BLACK);
	size_t i = 0;
	for(i = 0; i < displayPuffer.lines_cnt; i++){
		SSD1306_DrawLine(displayPuffer.lines[i].startX, displayPuffer.lines[i].startY, displayPuffer.lines[i].endX, displayPuffer.lines[i].endY, SSD1306_COLOR_WHITE);
	}
	//Text zeichnen
	for(i = 0; i < displayPuffer.texts_cnt; i++){
		SSD1306_GotoXY (displayPuffer.texts[i].x,displayPuffer.texts[i].y); // goto 10, 10
		SSD1306_Puts (displayPuffer.texts[i].str, displayPuffer.texts[i].Font, SSD1306_COLOR_WHITE); // print Hello
	}
	//Ausgabe nach außen
	SSD1306_UpdateScreen(); // update screen
	displayPuffer.lines_cnt = 0;
	displayPuffer.texts_cnt = 0;
	displayPuffer.changed = FALSE;
}

/**
 * Schreibt in die Bestenliste
 * Liest: score, name, aktAuswahl
 * Schreibt: datenschreiben
 */
void schreibeLead(void){
	static rangliste_daten_t resBuf;
	const game_result_daten_t *res_ptr = ranglistenArray->rd[aktAuswahl.index].daten;
	int i;
	if(aktAuswahl.menuItem->gamemode.moreIsBetter){
		for (i = 0; i < ranglistenArray->rd[aktAuswahl.index].data_cnt - 1; i++){
			memcpy(resBuf.daten + i, res_ptr, sizeof(game_result_daten_t));
			res_ptr++;
			if(res_ptr->measure < score) break;
		}
	} else {
		for (i = 0; i < ranglistenArray->rd[aktAuswahl.index].data_cnt - 1; i++){
			memcpy(resBuf.daten + i, res_ptr, sizeof(game_result_daten_t));
			res_ptr++;
			if(res_ptr->measure > score) break;
		}
	}
	resBuf.daten[i].measure = score;
	strcpy(resBuf.daten[i].name, name);
	for (; i < ranglistenArray->rd[aktAuswahl.index].data_cnt; i++){
		memcpy(resBuf.daten + i + 1, res_ptr, sizeof(game_result_daten_t));
		res_ptr++;
	}
	for(i = 0; i < NAME_LENGTH; i++) name[i] = 0;
	resBuf.data_cnt += resBuf.data_cnt < 10 ? 1 : 0;
	resBuf.gamemode_idx = aktAuswahl.menuItem->gamemode.gamemode_idx;
	HOF_writeNewData(&resBuf, &ranglistenArray);
}

/**
 * Schreibt auf dem Display
 * Liest: displayPuffer
 * Schreibt: bild
 */
void changeModeLead(void){
	curRanglistenPos = 0;
	curModeLead = (curModeLead + 1) % MODES_CNT;
	size_t titLen = sprintf(displayPuffer.texts[0].str, "%s", menuPunkte[ranglistenArray->rd[curModeLead].gamemode_idx].titel);
	size_t nameLen = sprintf(displayPuffer.texts[1].str, "%s", ranglistenArray->rd[curModeLead].daten[curRanglistenPos].name);
	size_t scoreLen = sprintf(displayPuffer.texts[2].str, "Score:%5lu", ranglistenArray->rd[curModeLead].daten[curRanglistenPos].measure);
	displayPuffer.texts[0].Font = &Font_16x26;
	displayPuffer.texts[0].x = (OLED_WIDTH / 2) - (titLen % 2 ? (OLED_CHAR_WIDTH_BIG_FONT / 2) : 0) - OLED_CHAR_WIDTH_BIG_FONT * (titLen / 2);
	displayPuffer.texts[0].y = 0;
	displayPuffer.texts[1].Font = &Font_11x18;
	displayPuffer.texts[1].x = (OLED_WIDTH / 2) - (nameLen % 2 ? (OLED_CHAR_WIDTH_NORMAL_FONT / 2) : 0) - OLED_CHAR_WIDTH_NORMAL_FONT * (nameLen / 2);
	displayPuffer.texts[1].y = OLED_CHAR_HEIGHT_BIG_FONT + 1;
	displayPuffer.texts[2].Font = &Font_11x18;
	displayPuffer.texts[2].x = (OLED_WIDTH / 2) - (scoreLen % 2 ? (OLED_CHAR_WIDTH_NORMAL_FONT / 2) : 0) - OLED_CHAR_WIDTH_NORMAL_FONT * (scoreLen / 2);
	displayPuffer.texts[2].y = OLED_CHAR_HEIGHT_BIG_FONT + OLED_CHAR_HEIGHT_NORMAL_FONT + 2;
	displayPuffer.texts_cnt = 3;
	displayPuffer.changed = TRUE;
}

/**
 * Hochscrollen des Leaderboards
 * Liest: curRanglistenPos, ranglistenArray, curModeLead
 * Schreibt: displayPuffer, curRanglistenPos
 */
void ranglisteScrollUp(void){
	if(curRanglistenPos > 0) curRanglistenPos--;
	size_t titLen = sprintf(displayPuffer.texts[0].str, "%s", menuPunkte[ranglistenArray->rd[curModeLead].gamemode_idx].titel);
	size_t nameLen = sprintf(displayPuffer.texts[1].str, "%s", ranglistenArray->rd[curModeLead].daten[curRanglistenPos].name);
	size_t scoreLen = sprintf(displayPuffer.texts[2].str, "Score:%5lu", ranglistenArray->rd[curModeLead].daten[curRanglistenPos].measure);
	displayPuffer.texts[0].Font = &Font_16x26;
	displayPuffer.texts[0].x = (OLED_WIDTH / 2) - (titLen % 2 ? (OLED_CHAR_WIDTH_BIG_FONT / 2) : 0) - OLED_CHAR_WIDTH_BIG_FONT * (titLen / 2);
	displayPuffer.texts[0].y = 0;
	displayPuffer.texts[1].Font = &Font_11x18;
	displayPuffer.texts[1].x = (OLED_WIDTH / 2) - (nameLen % 2 ? (OLED_CHAR_WIDTH_NORMAL_FONT / 2) : 0) - OLED_CHAR_WIDTH_NORMAL_FONT * (nameLen / 2);
	displayPuffer.texts[1].y = OLED_CHAR_HEIGHT_BIG_FONT + 1;
	displayPuffer.texts[2].Font = &Font_11x18;
	displayPuffer.texts[2].x = (OLED_WIDTH / 2) - (scoreLen % 2 ? (OLED_CHAR_WIDTH_NORMAL_FONT / 2) : 0) - OLED_CHAR_WIDTH_NORMAL_FONT * (scoreLen / 2);
	displayPuffer.texts[2].y = OLED_CHAR_HEIGHT_BIG_FONT + OLED_CHAR_HEIGHT_NORMAL_FONT + 2;
	displayPuffer.texts_cnt = 3;
	displayPuffer.changed = TRUE;
}

/**
 * Runterscrollen des Leaderboards
 * Liest: curRanglistenPos, ranglistenArray, curModeLead
 * Schreibt: displayPuffer, curRanglistenPos
 */
void ranglisteScrollDown(void){
	if(curRanglistenPos < ranglistenArray->rd[curModeLead].data_cnt - 1) curRanglistenPos++;
	size_t titLen = sprintf(displayPuffer.texts[0].str, "%s", menuPunkte[ranglistenArray->rd[curModeLead].gamemode_idx].titel);
	size_t nameLen = sprintf(displayPuffer.texts[1].str, "%s", ranglistenArray->rd[curModeLead].daten[curRanglistenPos].name);
	size_t scoreLen = sprintf(displayPuffer.texts[2].str, "Score:%5lu", ranglistenArray->rd[curModeLead].daten[curRanglistenPos].measure);
	displayPuffer.texts[0].Font = &Font_16x26;
	displayPuffer.texts[0].x = (OLED_WIDTH / 2) - (titLen % 2 ? (OLED_CHAR_WIDTH_BIG_FONT / 2) : 0) - OLED_CHAR_WIDTH_BIG_FONT * (titLen / 2);
	displayPuffer.texts[0].y = 0;
	displayPuffer.texts[1].Font = &Font_11x18;
	displayPuffer.texts[1].x = (OLED_WIDTH / 2) - (nameLen % 2 ? (OLED_CHAR_WIDTH_NORMAL_FONT / 2) : 0) - OLED_CHAR_WIDTH_NORMAL_FONT * (nameLen / 2);
	displayPuffer.texts[1].y = OLED_CHAR_HEIGHT_BIG_FONT + 1;
	displayPuffer.texts[2].Font = &Font_11x18;
	displayPuffer.texts[2].x = (OLED_WIDTH / 2) - (scoreLen % 2 ? (OLED_CHAR_WIDTH_NORMAL_FONT / 2) : 0) - OLED_CHAR_WIDTH_NORMAL_FONT * (scoreLen / 2);
	displayPuffer.texts[2].y = OLED_CHAR_HEIGHT_BIG_FONT + OLED_CHAR_HEIGHT_NORMAL_FONT + 2;
	displayPuffer.texts_cnt = 3;
	displayPuffer.changed = TRUE;
}

/**
 * Bereitet Daten zur Anzeige auf dem Display
 * Liest: name, nameIndex
 * Schreibt: displayPuffer
 */
void nameAnzeige(void){
	static size_t lastIndex = 255;
	strcpy(displayPuffer.texts[0].str, lastIndex == nameIndex ? "CHAR" : "NAME");
	strcpy(displayPuffer.texts[1].str, name);
	displayPuffer.texts[0].Font = &Font_16x26;
	displayPuffer.texts[0].x = (OLED_WIDTH / 2) - (4 % 2 ? (OLED_CHAR_WIDTH_BIG_FONT / 2) : 0) - OLED_CHAR_WIDTH_BIG_FONT * (4 / 2);
	displayPuffer.texts[0].y = 5;
	displayPuffer.texts[1].Font = &Font_11x18;
	displayPuffer.texts[1].x = (OLED_WIDTH / 2) - (NAME_LENGTH % 2 ? (OLED_CHAR_WIDTH_NORMAL_FONT / 2) : 0) - OLED_CHAR_WIDTH_NORMAL_FONT * (NAME_LENGTH / 2);
	displayPuffer.texts[1].y = OLED_HEIGHT - (OLED_HEIGHT - OLED_CHAR_HEIGHT_BIG_FONT - 5) / 2;
	displayPuffer.lines[0].startX = displayPuffer.texts[1].x + nameIndex * OLED_CHAR_WIDTH_NORMAL_FONT;
	displayPuffer.lines[0].endX = displayPuffer.texts[1].x + (nameIndex + 1) * OLED_CHAR_WIDTH_SMALL_FONT;
	displayPuffer.lines[0].startY = displayPuffer.lines[0].endY = (OLED_HEIGHT - (OLED_HEIGHT - OLED_CHAR_HEIGHT_BIG_FONT - 5) / 2) + OLED_CHAR_HEIGHT_NORMAL_FONT + 3;
	displayPuffer.lines_cnt = 1;
	displayPuffer.texts_cnt = 2;
	displayPuffer.changed = TRUE;
}

/**
 * Wählt nächstes Zeichen in Nameneingabe
 * Liest: nameIndex, name
 * Schreibt: name
 */
void nextChar(void){
	if(name[nameIndex] == 0) name[nameIndex] = 65;
	else if (name[nameIndex] > 64 && name[nameIndex] < 91)
		name[nameIndex] = name[nameIndex] == 90 ? 65 : name[nameIndex] + 1;
	else if (name[nameIndex] > 96 && name[nameIndex] < 123)
		name[nameIndex] = name[nameIndex] == 122 ? 97 : name[nameIndex] + 1;
}

/**
 * Wählt letztes Zeichen in Nameneingabe
 * Liest: nameIndex, name
 * Schreibt: name
 */
void lastChar(void){
	if(name[nameIndex] == 0) name[nameIndex] = 90;
	else if (name[nameIndex] > 64 && name[nameIndex] < 91)
		name[nameIndex] = name[nameIndex] == 65 ? 90 : name[nameIndex] - 1;
	else if (name[nameIndex] > 96 && name[nameIndex] < 123)
		name[nameIndex] = name[nameIndex] == 97 ? 122 : name[nameIndex] - 1;
}

/**
 * Wechsel zwischen Groß- und Kleinbuchstaben oder Löschung
 * Liest: nameIndex, name
 * Schreibt: name
 */
void capsChar(void){
	if(name[nameIndex] == 0) name[nameIndex] = 65;
	else if (name[nameIndex] > 64 && name[nameIndex] < 91)
		name[nameIndex] += 32;
	else if (name[nameIndex] > 96 && name[nameIndex] < 123)
		name[nameIndex] = name[nameIndex + 1] == 0 ? 0 : name[nameIndex] - 32;
}

/**
 * Geht zur letzten Position im Namen
 * Liest: nameIndex
 * Schreibt: nameIndex
 */
void prevNamePos(void){
	if (nameIndex) nameIndex--;
}

/**
 * Geht zur nächsten Position im Namen
 * Liest: nameIndex
 * Schreibt: nameIndex
 */
void nextNamePos(void){
	if(name[nameIndex] && nameIndex < NAME_LENGTH) nameIndex++;
}

/**
 * Setzt das Menü zurück
 * Schreibt: aktAuswahl, displayPuffer
 */
void resetMenu(void){
	aktAuswahl.index = 0;
	aktAuswahl.menuItem = menuPunkte;
}
