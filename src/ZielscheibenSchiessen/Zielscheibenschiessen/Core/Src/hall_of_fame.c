/*
 * hall_of_fame.c
 *
 *  Created on: 11.01.2020
 *      Author: Patryk
 */
#include "hall_of_fame.h"
#include "stm32f4xx_hal.h"

__attribute__((__section__(".user_data1")))   uint8_t arr8Sec1[FLASH_SIZE_BYTE];
__attribute__((__section__(".user_data2")))   uint8_t arr8Sec2[FLASH_SIZE_BYTE];
uint16_t *arr16Sec1 = (uint16_t*) arr8Sec1;
uint16_t *arr16Sec2 = (uint16_t*) arr8Sec2;
uint32_t *arr32Sec1 = (uint32_t*) arr8Sec1;
uint32_t *arr32Sec2 = (uint32_t*) arr8Sec2;
ranglisten_arr_t *arrRangSec1 = (ranglisten_arr_t*) arr8Sec1;
ranglisten_arr_t *arrRangSec2 = (ranglisten_arr_t*) arr8Sec2;
uint8_t *arrFullFlag1 = arr8Sec1 + FLASH_SIZE_RANG * RANG_SIZE;
uint8_t *arrFullFlag2 = arr8Sec2 + FLASH_SIZE_RANG * RANG_SIZE;

enum {
	FIRST, SECOND
} act_sector;

bool_t initialized = FALSE;

uint16_t act_idx;

uint32_t HOF_calcChecksumNew(const rangliste_daten_t *data,
		const ranglisten_arr_t *ra) {
	static uint32_t *CRC_DR = (uint32_t*) 0x40023000;
	static uint32_t *CRC_CR = (uint32_t*) 0x40023008;
	*CRC_CR = 1;
	uint32_t *ra_arr = (uint32_t*) ra->rd;
	uint32_t *data_arr = (uint32_t*) data;
	int i;
	uint8_t mode = 0, base = 0;
	for (i = 0;
			i
					< (sizeof(ra->rd) / sizeof(uint32_t))
							+ (sizeof(ra->rd) % sizeof(uint32_t) == 0 ? 0 : 1);
			i += 4) {
		if (mode == data->gamemode_idx
				&& sizeof(rangliste_daten_t) - i * 32 - base * 8 >= 32) {
			*CRC_DR = data_arr[(base + i) % sizeof(rangliste_daten_t)];
		} else if (mode != data->gamemode_idx
				&& sizeof(rangliste_daten_t) - i * 32 - base * 8 >= 32)
			*CRC_DR = ra_arr[base + i];
		else {
			uint8_t cntOfOld, bytePos = 0;
			uint32_t crc_data = 0;
			if (mode == data->gamemode_idx) {
				cntOfOld = (sizeof(rangliste_daten_t) - i * 32 - base * 8) / 8;
				for (; bytePos < cntOfOld; bytePos++)
					crc_data += data_arr[i % sizeof(rangliste_daten_t)]
							& (0x000000FFU << (3 - bytePos));
				base = bytePos;
				bytePos--;
				for (; bytePos < 4; bytePos++)
					crc_data += ra_arr[base + i]
							& (0x000000FFU << (3 - bytePos));
			} else {
				cntOfOld = (sizeof(rangliste_daten_t) - i * 32 - base * 8) / 8;
				for (; bytePos < cntOfOld; bytePos++)
					crc_data += ra_arr[i] & (0x000000FFU << (3 - bytePos));
				base = bytePos;
				bytePos--;
				for (; bytePos < 4; bytePos++)
					crc_data += data_arr[(base + i) % sizeof(rangliste_daten_t)]
							& (0x000000FFU << (3 - bytePos));
			}
			*CRC_DR = crc_data;
		}
	}
	return *CRC_DR;
}

uint32_t HOF_calcChecksum(const ranglisten_arr_t *ra) {
	static uint32_t *CRC_DR = (uint32_t*) 0x40023000;
	static uint32_t *CRC_CR = (uint32_t*) 0x40023008;
	*CRC_CR = 1;
	uint32_t *ra_arr = (uint32_t*) ra->rd;
	int i;
	for (i = 0;
			i
					< (sizeof(ra->rd) / sizeof(uint32_t))
							+ (sizeof(ra->rd) % sizeof(uint32_t) == 0 ? 0 : 1);
			i++) {
		*CRC_DR = ra_arr[i];
	}
	return *CRC_DR;
}

const ranglisten_arr_t* HOF_writeNewDataNewSector(const rangliste_daten_t *data,
		const ranglisten_arr_t **ra) {
	uint32_t checksum_orig;
	uint8_t *ra_arr = (uint8_t*) *ra;
	uint8_t *data_arr = (uint8_t*) data;
	FLASH_EraseInitTypeDef eraseStruct;
	eraseStruct.Sector = act_sector == FIRST ? FLASH_SECTOR_7 : FLASH_SECTOR_6;
	eraseStruct.NbSectors = 1;
	eraseStruct.VoltageRange = FLASH_VOLTAGE_RANGE_3;
	eraseStruct.TypeErase = FLASH_TYPEERASE_SECTORS;
	uint32_t eraseError;
	HAL_FLASH_Unlock();
	__HAL_FLASH_CLEAR_FLAG(
			FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR);
	if (HAL_FLASHEx_Erase(&eraseStruct, &eraseError) == HAL_ERROR)
		return 0;
	checksum_orig = HOF_calcChecksumNew(data, *ra);
	act_idx = 1;
	act_sector = act_sector == FIRST ? SECOND : FIRST;
	int byteIndex = 0;
	uint8_t mode;
	const ranglisten_arr_t *startAddress =
			act_sector == FIRST ? arrRangSec1 : arrRangSec2;
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE,
			(uint32_t) ((void*) startAddress) + byteIndex, data_arr[0]);
	byteIndex++;
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE,
			(uint32_t) ((void*) startAddress) + byteIndex, data_arr[1]);
	byteIndex++;
	for (mode = 0; mode < sizeof((*ra)->rd); mode++)
		if (mode == data->gamemode_idx) {
			int i;
			for (i = 0; i < sizeof((*ra)->rd); i++, byteIndex++)
				HAL_FLASH_Program(TYPEPROGRAM_BYTE,
						(uint32_t) ((void*) startAddress) + byteIndex,
						data_arr[i]);
		} else
			for (; byteIndex % (sizeof((*ra)->rd) + 1) < sizeof((*ra)->rd);
					byteIndex++)
				HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE,
						(uint32_t) ((void*) startAddress) + byteIndex,
						ra_arr[byteIndex]);
	HAL_FLASH_Program(TYPEPROGRAM_WORD,
			(uint32_t) ((void*) startAddress) + byteIndex, checksum_orig);
	switch (act_sector) {
	case FIRST:
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE,
				(uint32_t) arrFullFlag1 + (act_idx - 1) / 8,
				arrFullFlag1[act_idx - 1 / 8] >> 1);
		break;
	case SECOND:
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE,
				(uint32_t) arrFullFlag2 + (act_idx - 1) / 8,
				arrFullFlag2[act_idx - 1 / 8] >> 1);
		break;
	}
	HAL_FLASH_Lock();
	if (checksum_orig != HOF_calcChecksum(startAddress)
			|| checksum_orig != startAddress->crc_checksum)
		return 0;
	*ra = startAddress;
	return startAddress;
}

bool_t initHOF() {
	int i;
	for (i = 0; i < FLASH_SIZE_FULL_FLAG; i++) {
		if (arrFullFlag1[i] != arrFullFlag2[i]) {
			if (arrFullFlag1[i] < arrFullFlag2[i])
				act_sector = FIRST;
			else if (arrFullFlag1[i] > arrFullFlag2[i])
				act_sector = SECOND;
			act_idx = i * 8;
			uint8_t j = 0xFF;
			switch (act_sector) {
			case FIRST:
				for (j = 0xFF; arrFullFlag1[i] != j; act_idx++, j >>= 1)
					;

				break;
			case SECOND:
				for (j = 0xFF; arrFullFlag2[i] != j; act_idx++, j >>= 1)
					;
				break;
			}
		}
	}
	initialized = TRUE;
	return initialized;
}

const ranglisten_arr_t* HOF_writeNewData(const rangliste_daten_t *data,
		const ranglisten_arr_t **ra) {
	uint32_t checksum_orig;
	uint8_t *ra_arr = (uint8_t*) *ra;
	uint8_t *data_arr = (uint8_t*) data;
	HAL_FLASH_Unlock();
	if (act_idx >= FLASH_SIZE_RANG)
		return HOF_writeNewDataNewSector(data, ra);
	checksum_orig = HOF_calcChecksumNew(data, *ra);
	int byteIndex = 0;
	uint8_t mode;
	const ranglisten_arr_t *startAddress =
			act_sector == FIRST ? arrRangSec1 + act_idx : arrRangSec2 + act_idx;
	act_idx++;
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE,
			(uint32_t) ((void*) startAddress) + byteIndex, data_arr[0]);
	byteIndex++;
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE,
			(uint32_t) ((void*) startAddress) + byteIndex, data_arr[1]);
	byteIndex++;
	for (mode = 0; mode < sizeof((*ra)->rd); mode++)
		if (mode == data->gamemode_idx) {
			int i;
			for (i = 0; i < sizeof((*ra)->rd); i++, byteIndex++)
				HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE,
						(uint32_t) ((void*) startAddress) + byteIndex,
						data_arr[i]);
		} else
			for (; byteIndex % (sizeof((*ra)->rd) + 1) < sizeof((*ra)->rd);
					byteIndex++)
				HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE,
						(uint32_t) ((void*) startAddress) + byteIndex,
						ra_arr[byteIndex]);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD,
			(uint32_t) ((void*) startAddress) + byteIndex, checksum_orig);
	switch (act_sector) {
	case FIRST:
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE,
				(uint32_t) arrFullFlag1 + (act_idx - 1) / 8,
				arrFullFlag1[act_idx - 1 / 8] >> 1);
		break;
	case SECOND:
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE,
				(uint32_t) arrFullFlag2 + (act_idx - 1) / 8,
				arrFullFlag2[act_idx - 1 / 8] >> 1);
		break;
	}
	HAL_FLASH_Lock();
	if (checksum_orig != HOF_calcChecksum(startAddress)
			|| checksum_orig != startAddress->crc_checksum)
		return 0;
	*ra = startAddress;
	return startAddress;
}

const ranglisten_arr_t* HOF_getCurDataPos() {
	return (act_sector == FIRST ? arrRangSec1 + act_idx : arrRangSec2 + act_idx)
			- 1;
}
