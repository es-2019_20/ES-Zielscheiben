#include "pat.h"
#include "process.h"
#include "control_data.h"

/**
 * Prozessaktivierungstabelle
 */
void pat(){
	switch(aktion){
		case A_SHOW_MENU:
			resetMenu();
			doAction();
			displayRefresh();
		break;
		case A_IDLE:
		break;
		case A_SEL_NEXT:
			nextMenuPunkt();
			doAction();
			displayRefresh();
		break;
		case A_SEL_PREV:
			lastMenuPunkt();
			doAction();
			displayRefresh();
		break;
		case A_VOLUME:
			volumeAnzeige();
			displayRefresh();
		break;
		case A_VOLUME_DOWN:
			lautstaerkeDown();
			soundAn();
			volumeAnzeige();
			displayRefresh();
		break;
		case A_VOLUME_UP:
			lautstaerkeUp();
			soundAn();
			volumeAnzeige();
			displayRefresh();
		break;
		case A_START:
			leseHelligkeit();
			schwellenWertBerechnen();
			startGame();
			schreibeServoWinkel();
			gameAnzeige();
			displayRefresh();
		break;
		case A_FIRE:
			laserAn();
			leseHelligkeit();
			entscheideTreffer();
			game();
			schreibeServoWinkel();
			gameAnzeige();
			displayRefresh();
		break;
		case A_GAME:
			laserAus();
			leseHelligkeit();
			schwellenWertBerechnen();
			game();
			schreibeServoWinkel();
			gameAnzeige();
			displayRefresh();
		break;
		case A_FIN:
			laserAus();
			gameAnzeige();
			displayRefresh();
		break;
		case A_FIN_BEST:
			laserAus();
			nameAnzeige();
			displayRefresh();
		break;
		case A_NEXT_NAME_POS:
			nextNamePos();
			nameAnzeige();
			displayRefresh();
		break;
		case A_PREV_NAME_POS:
			prevNamePos();
			nameAnzeige();
			displayRefresh();
		break;
		case A_CHAR:
			nameAnzeige();
			displayRefresh();
		break;
		case A_CHAR_END:
			nameAnzeige();
			displayRefresh();
		break;
		case A_NEXT_CHAR:
			nextChar();
			nameAnzeige();
			displayRefresh();
		break;
		case A_PREV_CHAR:
			lastChar();
			nameAnzeige();
			displayRefresh();
		break;
		case A_CAPS_CHAR:
			capsChar();
			nameAnzeige();
			displayRefresh();
		break;
		case A_SCHREIBE_LEAD:
			schreibeLead();
			resetMenu();
			doAction();
			displayRefresh();
		break;
		case A_CHANGE_MODE_LEAD:
			changeModeLead();
			displayRefresh();
		break;
		case A_SCROLL_DOWN:
			ranglisteScrollDown();
			displayRefresh();
		break;
		case A_SCROLL_UP:
			ranglisteScrollUp();
			displayRefresh();
		break;
	}
}
