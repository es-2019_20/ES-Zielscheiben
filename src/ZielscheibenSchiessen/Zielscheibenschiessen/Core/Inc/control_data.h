#ifndef CONTROL_DATA_H
#define CONTROL_DATA_H
#include "types.h"

//Zustand des Automaten
extern zustand_t zustand;

//Aktion des System
extern aktion_t aktion;

/* Kontrollflüssse
 * reset - für Reset
 * fin - für Spielende
 * rotR - für Rechtsdrehung am RotaryEncoder
 * rotL - für Linksdrehung am RotaryEncoder
 * rotOK - für Button am RotaryEncoder
 * fire - für den Abzug an der Laserpistole
 * changeVolume - für Menüwechsel zu Volume
 * showLeaderboard - für Menüwechsel zu Score
 * isBest - für neuen Highscore
 * start - für Menüwechsel zum Game
 */
extern bool_t reset, fin, rotR, rotL, rotOK, fire, changeVolume, showLeaderboard, isBest, start;

/**
 * Räumt die Kontrollflüsse reset, fin und isBest auf.
 */
static inline void clear_control(){
	reset = fin = isBest = 0;
}

#endif
