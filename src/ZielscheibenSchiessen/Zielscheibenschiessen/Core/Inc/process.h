#ifndef PROCESS_H
#define PROCESS_H
#include "main.h"

/**
 * Initialisiert die Prozesse, die Hardwarezugriff benötigen
 * Alle anderen Prozesse sind stehts Initialisiert
 * Vor dem ersten Aufruf der Prozessaktivierungstabelle aufrufen
 */
void init(ADC_HandleTypeDef *_hadc1, TIM_HandleTypeDef *_htim2, TIM_HandleTypeDef *_htim3, TIM_HandleTypeDef *_htim4, I2C_HandleTypeDef *_hi2c1, UART_HandleTypeDef *_music_uart_handle, UART_HandleTypeDef *_fx_uart_handle);

/**
 * Berechnet das Schwellenwert
 * Liest: umgebungsHelligkeit
 * Schreibt: schwellenWert
 */
void schwellenWertBerechnen(void);

/**
 * Liest Helligkeiten
 * Liest: hellwertzielX, umhellwertX
 * Schreibt: umgebungsHelligkeit, zielHelligkeit
 */
void leseHelligkeit(void);

/**
 * Entscheidet über Treffer
 * Liest: zielHelligkeit, schwellenWert, lastUp
 * Schreibt: lastUp, treffer
 */
void entscheideTreffer(void);

/**
 * Hauptlogik eines Spiels
 * Berechnet die Time Delta zwischen Durchläufen
 * Prüft die Spielendebedingungen
 * Berechnet die Punkte
 * Prüft auf Bestenlisteneintrag
 * Zählt Scheiben die Oben sind
 * Klappt Scheiben unter bestimmten Bedingungen hoch
 * Reagiert auf Treffer
 * Liest: startTime, treffer, alteTreffer, laserInfo, servoDown, lastTimeStamp, trefferSum, oneUpAtATimeLastUp, aktZeit, aktAuswahl
 * Schreibt: alteTreffer, servoDown, lastTimeStamp, trefferSum, oneUpAtATimeLastUp, aktZeit, score, fin, isBest, soundauswahl, fxauswahl
 */
void game(void);

/**
 * Initialisiert das Spiel
 * Bringt Scheiben in die Initialposition
 * Setzt Treffer, Score, Laserdaten und timestamps zurück
 * Liest: aktAuswahl
 * Schreibt: alteTreffer, treffer, servoDown, lastTimeStamp, trefferSum, oneUpAtATimeLastUp, aktZeit, score
 */
void startGame(void);

/**
 * Bewegt Servos
 * Liest: servoDown
 * Schreibt: winkelservoX
 */
void schreibeServoWinkel(void);

/**
 * Erhöht die Lautstärke
 * Liest: lautstaerke
 * Schreibt: lautstaerke
 */
void lautstaerkeUp(void);

/**
 * Verkleinert die Lautstärke
 * Liest: lautstaerke
 * Schreibt: lautstaerke
 */
void lautstaerkeDown(void);

/**
 * Schaltet den Laser an wenn der aktuelle Zustand es zulässt
 * Liest: aktAuswahl, laserInfo
 * Schreibt: laserInfo, laser
 */
void laserAn(void);

/**
 * Schaltet den Laser aus
 * Liest: aktAuswahl, laserInfo
 * Schreibt: laserInfo, laser
 */
void laserAus(void);

/**
 * Überträgt die Lautstärke auf das Audiomodul
 * Liest: lautstaerke
 * Schreibt: lautstaerkeModul
 */
void soundAn(void);

/**
 * Setzt Kontrollflüsse je nach aktueller Menüauswahl
 * Liest: aktAuswahl
 * Schreibt: start, showLeaderboard, changeVolume
 */
void doAction(void);

/**
 * Wechselt zum nächsten Menüpunkt
 * Liest: menuPunkte, aktAuswahl
 * Schreibt: aktAuswahl, displayPuffer
 */
void nextMenuPunkt(void);

/**
 * Wechselt zum letzten Menüpunkt
 * Liest: menuPunkte, aktAuswahl
 * Schreibt: aktAuswahl, displayPuffer
 */
void lastMenuPunkt(void);

/**
 * Setzt das Menü zurück
 * Schreibt: aktAuswahl, displayPuffer
 */
void resetMenu(void);

/**
 * Bereitet Daten zur Anzeige auf dem Display
 * Liest: aktZeit, score, aktAuswahl
 * Schreibt: displayPuffer
 */
void gameAnzeige(void);

/**
 * Bereitet Daten zur Anzeige auf dem Display
 * Liest: lautstaerke
 * Schreibt: displayPuffer
 */
void volumeAnzeige(void);

/**
 * Bereitet Daten zur Anzeige auf dem Display
 * Liest: name, nameIndex
 * Schreibt: displayPuffer
 */
void nameAnzeige(void);

/**
 * Berechnet das Schwellenwert
 * Liest: umgebungsHelligkeit
 * Schreibt: schwellenWert
 */
void displayRefresh(void);

/**
 * Schreibt auf dem Display
 * Liest: displayPuffer
 * Schreibt: bild
 */
void changeModeLead(void);

/**
 * Hochscrollen des Leaderboards
 * Liest: curRanglistenPos, ranglistenArray, curModeLead
 * Schreibt: displayPuffer, curRanglistenPos
 */
void ranglisteScrollUp(void);

/**
 * Runterscrollen des Leaderboards
 * Liest: curRanglistenPos, ranglistenArray, curModeLead
 * Schreibt: displayPuffer, curRanglistenPos
 */
void ranglisteScrollDown(void);

/**
 * Schreibt in die Bestenliste
 * Liest: score, name, aktAuswahl
 * Schreibt: datenschreiben
 */
void schreibeLead(void);

/**
 * Wählt nächstes Zeichen in Nameneingabe
 * Liest: nameIndex, name
 * Schreibt: name
 */
void nextChar(void);

/**
 * Wählt letztes Zeichen in Nameneingabe
 * Liest: nameIndex, name
 * Schreibt: name
 */
void lastChar(void);

/**
 * Wechsel zwischen Groß- und Kleinbuchstaben oder Löschung
 * Liest: nameIndex, name
 * Schreibt: name
 */
void capsChar(void);

/**
 * Geht zur letzten Position im Namen
 * Liest: nameIndex
 * Schreibt: nameIndex
 */
void prevNamePos(void);

/**
 * Geht zur nächsten Position im Namen
 * Liest: nameIndex
 * Schreibt: nameIndex
 */
void nextNamePos(void);

#endif
