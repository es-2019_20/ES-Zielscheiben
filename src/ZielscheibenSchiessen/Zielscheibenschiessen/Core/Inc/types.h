#ifndef TYPES_H
#define TYPES_H

// Anzahl Gamemodus
#define MODES_CNT 3

//defines für OLED-Display
#define OLED_WIDTH 128
#define OLED_HEIGHT 64
#define OLED_CHAR_WIDTH_SMALL_FONT 7
#define OLED_CHAR_WIDTH_NORMAL_FONT 11
#define OLED_CHAR_WIDTH_BIG_FONT 16
#define OLED_CHAR_HEIGHT_SMALL_FONT 10
#define OLED_CHAR_HEIGHT_NORMAL_FONT 18
#define OLED_CHAR_HEIGHT_BIG_FONT 26
#define OLED_MAX_CHARS_SMALL_FONT (OLED_WIDTH / OLED_CHAR_WIDTH_SMALL_FONT)
#define OLED_MAX_CHARS_NORMAL_FONT (OLED_WIDTH / OLED_CHAR_WIDTH_NORMAL_FONT)
#define OLED_MAX_CHARS_BIG_FONT (OLED_WIDTH / OLED_CHAR_WIDTH_BIG_FONT)
#define OLED_MAX_LINES_SMALL_FONT (OLED_HEIGHT / OLED_CHAR_HEIGHT_SMALL_FONT)
#define OLED_MAX_LINES_NORMAL_FONT (OLED_HEIGHT / OLED_CHAR_HEIGHT_NORMAL_FONT)
#define OLED_MAX_LINES_BIG_FONT (OLED_HEIGHT / OLED_CHAR_HEIGHT_BIG_FONT)
#define OLED_MAX_DRAW_LINES_COUNT 10

//Anzahl Umgebungsfotowiderstaende
#define ENVIRONMENT_SENSORS_COUNT 2
//Anzahl Ziele
#define TARGET_COUNT 5
//Anzahl Zielfotowiderstaende
#define TARGET_SENSORS_COUNT TARGET_COUNT
//Anzahl Servos
#define TARGET_SERVOS_COUNT TARGET_COUNT
//Größe des Ringpuffers für Schwellenwert
#define THRESHHOLD_RING_COUNT 10

//Maximale Länge des Namens
#define NAME_LENGTH OLED_MAX_CHARS_NORMAL_FONT

//Länge der Spielhistorie
#define GAME_DATEN_HISTORY_LENGTH 10

//Maximaler Volumewert
#define VOLUME_MAX 30
//Minimaler Volumewert
#define VOLUME_MIN 0
//Initialer Volumewert
#define VOLUME_INIT_VAL 10

#include <stdint.h>
#include <stddef.h>
#include "fonts.h"

/**
 * Alle definierten Zustände:
 * 	Z_MENU - Menüauswahl
 *	Z_VOLUME - Lautstärkewahl
 *	Z_GAME - Spiel
 *	Z_GAME_ENDE - Spielende
 *	Z_GAME_ENDE_BEST - Spielende mit neuen Highscore
 *	Z_BUCHSTABE - Spielnameneingabe
 *	Z_LEADERBOARD - Ranglistenanzeige
 */
typedef enum {
	Z_MENU,
	Z_VOLUME,
	Z_GAME,
	Z_GAME_ENDE,
	Z_GAME_ENDE_BEST,
	Z_BUCHSTABE,
	Z_LEADERBOARD
} zustand_t;

/**
 * Alle definierten Aktionen:
 *  A_SHOW_MENU - Zeige das Menü im Default
 *	A_IDLE - Idle
 *	A_SEL_NEXT - Wechsel zum nächsten Menpunkt
 *	A_SEL_PREV - Wechsel zum vorigen Menpunkt
 *	A_VOLUME  - Wechsel zur Lautstaerkeänderung
 *	A_VOLUME_DOWN - Verringere Lautstaerke
 *	A_VOLUME_UP - Verstaerkere Lautstaerke
 *	A_START - Bereite alles für den Spielstart vor
 *	A_FIRE - Spielablauf mit Laserpistole gedrueckt
 *	A_GAME - Spielablauf
 *	A_FIN - Spielende erreicht
 *	A_FIN_BEST - Spielende und neuen HighScore erreicht
 *	A_NEXT_NAME_POS - Wechsel zur nächsten Position im Namen
 *	A_PREV_NAME_POS - Wechsel zur vorigen Position im Namen
 *	A_CHAR - Wechsel für den Nameneingabe
 *	A_CHAR_END - Ende der Namenseingabe
 *	A_NEXT_CHAR - Wechsel zum nächsten Zeichen an aktueller Position im Namen
 *	A_PREV_CHAR - Wechsel zum vorigen Zeichen an aktueller Position im Namen
 *	A_CAPS_CHAR - Wechsel von Klein- zu Grossbuchstaben an aktueller Position im Namen
 *	A_SCHREIBE_LEAD - Schreibt den eingegebenen Namen in den Speicher
 *	A_CHANGE_MODE_LEAD - Wechselt die Scoreliste
 *	A_SCROLL_DOWN - Runterscrollen der aktuellen Scoreliste
 *	A_SCROLL_UP - Hochscrollen der aktuellen Scoreliste
 */
typedef enum {
	A_SHOW_MENU,
	A_IDLE,
	A_SEL_NEXT,
	A_SEL_PREV,
	A_VOLUME,
	A_VOLUME_DOWN,
	A_VOLUME_UP,
	A_START,
	A_FIRE,
	A_GAME,
	A_FIN,
	A_FIN_BEST,
	A_NEXT_NAME_POS,
	A_PREV_NAME_POS,
	A_CHAR,
	A_CHAR_END,
	A_NEXT_CHAR,
	A_PREV_CHAR,
	A_CAPS_CHAR,
	A_SCHREIBE_LEAD,
	A_CHANGE_MODE_LEAD,
	A_SCROLL_DOWN,
	A_SCROLL_UP
} aktion_t;

//Bool typedef
typedef enum { FALSE = 0, TRUE = 1 } bool_t;

//Typedefs für ints
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef signed char int8_t;
typedef signed short int16_t;

//Fonts
typedef enum {
	SMALL,
	NORMAL,
	BIG
} font_t;

/**
 * Struktur für Text:
 * x,y - Koordinate, wo es angezeit werden soll (oben links im Display(0,0))
 * len - Länge des Textes
 * Font - Fonteinstellung
 * str - der Textstring
 */
typedef struct {
	uint8_t x;
	uint8_t y;
	//size_t len;
	FontDef_t* Font;
	char str[OLED_MAX_CHARS_SMALL_FONT + 1];
} text_t;

/**
 * Struktur für Linien auf dem Display:
 * startX,startY - Startkoordinate
 * endX,endY - Endkoordinate
 */
typedef struct {
	uint8_t startX;
	uint8_t startY;
	uint8_t endX;
	uint8_t endY;
} line_t;

/**
 * Struktur für Daten die auf dem Display angezeigt werden sollen:
 * texts - der Text
 * texts_cnt - Textlänge
 * lines - die Linien
 * lines_cnt - Länge der Linien
 * changed - Boolvariable, ob sich etwas verändert hat
 */
typedef struct {
	text_t texts[OLED_MAX_LINES_SMALL_FONT];
	size_t texts_cnt;
	line_t lines[OLED_MAX_DRAW_LINES_COUNT];
	size_t lines_cnt;
	bool_t changed;
} display_buffer_t;

/**
 * Struktur für ein Ergebniseintrag:
 * measure - das Score
 * name - Spielername
 */
typedef struct {
	uint32_t measure;
	char name[NAME_LENGTH];
} game_result_daten_t;

/**
 * Struktur für eine Rangliste für ein Gamemodi:
 * gamemode_idx - Gameindex
 * datacnt - Anzahl der Datensätze
 * daten - Datensätze (Eine Liste hat 10 Datensätze)
 */
typedef struct {
	uint8_t gamemode_idx;
	uint8_t data_cnt;
	game_result_daten_t daten[GAME_DATEN_HISTORY_LENGTH];
} rangliste_daten_t;

/**
 * Struktur für Rangliste:
 * rd - Array mit den Ranglisten für jedes Gamemodi
 * crc_checksum - fuer CRC Pruefung lesen/schreiben aus Speicher
 */
typedef struct {
	rangliste_daten_t rd[MODES_CNT];
	uint32_t crc_checksum;
} ranglisten_arr_t;

/**
 * Struktur für Zustand der Laserpistole:
 * isAn - true->Laser ist an, false->Laserr ist aus
 * timestamp - zaehlt hoch wie lange der Laser schon an ist
 * shotsCounter - zaehlt hoch wie oft der Abzug schon gedrückt wurde
 */
typedef struct {
	bool_t isAn;
	uint32_t timeStamp; // millisekunden
	uint32_t shotsCounter;
} laser_daten_t;

/**
 * Struktur für die Laserkonfiguration:
 * maxTime  - wie lange der Laser an sein darf
 * maxShots - wie oft der Abzug gedrueckt werden darf
 */
typedef struct {
	uint32_t maxTime; // sekunden
	uint32_t maxShots;
} laser_conf_t;

/**
 * enum für das Servoverhalten:
 * NONE - bei Zieltreffer bleib das Ziel unten
 * RAND - Servos klappen nach einer zufälliger Zeit wieder hoch (sehe servo_conf_t)
 * FIFO - Servos klappen in einer FIFO weise nach einer festen Zeit wieder hoch (sehe servo_conf_t)
 * ONE_UP_RAND - nur ein zufaelliges Ziel ist oben
 * ALL - Wenn alle Zielscheiben unten sind klappen alle wieder hoch
 */
typedef enum {
	NONE,
	RAND,
	FIFO,
	ONE_UP_RAND,
	ALL
} next_servo_up_t;

/**
 * Struktur fuer die Konfiguration der Servos:
 * nextServoUpConf -  Servoverhalten
 * minServoDownTime -  Wie lange das Ziel minimal(FIFO, RAND) unten sein soll
 * maxServoDownTime - Wie lange das Ziel maximal(RAND) unten sein soll
 */
typedef struct {
	next_servo_up_t nextServoUpConf;
	uint32_t minServoDownTime;
	uint32_t maxServoDownTime;
} servo_conf_t;

/**
 * Typedef für einen Scoreberechnungsdelegaten
 */
typedef uint32_t (*getScore_t)(uint32_t zeit, uint8_t treffer[], size_t targetsCnt);

/**
 * Struktur für Gamemodus:
 * 	 gamemode_idx - Gameindex
 *	 laserConf - Konfiguration Laserpistole
 *	 servoConf - Konfiguration Servos
 *	 maxTreffer - maximal erlaubte Treffer; 0 = unendlich
 *	 maxTime - maximale Spielzeit; 0 = unendlich
 *	 moreIsBetter - TRUE - Höherer Score -> Besser; FALSE - Niedrigerer Score -> Besser
 *	 getScore - Delegat für Scoreberechnung
 */
typedef struct {
	uint8_t gamemode_idx;
	laser_conf_t laserConf;
	servo_conf_t servoConf;
	uint32_t maxTreffer;
	uint32_t maxTime;
	bool_t moreIsBetter;
	getScore_t getScore;
} gamemode_t;

/**
 * Es gibt 3 Types für Menuepunkte:
 * GAMEMODE - Menuepunkt ist Spielmodus
 * VOLUME - Menuepunkt ist Lautstaerkeeinstellung
 * LEADERBOARD - Menuepunkt ist Scoreliste
 */
typedef enum {
	GAMEMODE,
	VOLUME,
	LEADERBOARD
} menu_item_content_t;

/**
 * Struktur fuer Menuepunkte:
 * menuItemType - Typ des Menuepunktes
 * gamemode - Konfiguration des Gamemodus, wenn Menuepunkt Gamemode ist
 * titel - Titel
 */
typedef struct {
	menu_item_content_t menuItemType;
	gamemode_t gamemode;
	char titel[OLED_MAX_CHARS_SMALL_FONT];
} menu_item_t;

/**
 * Struktur für aktuelle Auswahl:
 * index - Menüindex
 * menuItem - Pointer auf das zum Index gehoerige Menuepunkt
 */
typedef struct {
	int8_t index;
	menu_item_t *menuItem;
} akt_menu_item_t;

#endif
