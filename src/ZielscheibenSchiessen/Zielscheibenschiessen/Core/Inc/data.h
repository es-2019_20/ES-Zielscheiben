#ifndef DATA_H
#define DATA_H
#include "types.h"

/**
 * Datenspeicher für Array mit allen Menüpunkten
 */
extern menu_item_t menuPunkte[];

/**
 * Datenspeicher für Aktuelle Auswahl im Menü
 * Hält den Index des Eintrags
 * und einen Pointer auf auf dem Menü Eintrag in menuPunkte[]
 */
extern akt_menu_item_t aktAuswahl;

/**
 * Datenspeicher für Lautstärke
 */
extern uint8_t lautstaerke;

/**
 * Datenspeicher für Buffern von Displaydaten
 */
extern display_buffer_t displayPuffer;

/**
 * Datenspeicher für die Spielernameneingabe am Ende des Spiels
 */
extern char name[NAME_LENGTH + 1];

/**
 * Datenspeicher für das Speichern der Umgebungshelligkeitswerten
 * Zweidimensionales Array von uint32 Werten
 * Pro einen Sensor werden 10 Werte festegehalten aus denen,
 * dann ein Durchschnittswert für Schwellenwert berechnung benutzt werden kann
 * Zugriff über [Sensorindex][Wertindex]
 */
uint32_t umgebungsHelligkeit[ENVIRONMENT_SENSORS_COUNT][THRESHHOLD_RING_COUNT];

/**
 * Datenspeicher für das Speichern der Zielscheibenhelligkeitswerten
 */
uint32_t zielHelligkeit[TARGET_SENSORS_COUNT];

/**
 * Datenspeicher für den Schwellenwert für Treffererkennung
 */
uint32_t schwellenWert;

/**
 * Datenspeicher für Festhalten des aktuell getroffenen Zielen
 * Vermeidung von doppelten Treffererkennungen
 */
bool_t lastUp[TARGET_COUNT];

/**
 * Datenspeicher für das Festhalten der Treffer
 * Pro Zielscheibe wird hier ein Zähler festgehalten
 * Ein vergleich mit alteTreffer führt zur Erkennung eines Trefferereignisses
 */
uint8_t treffer[TARGET_COUNT];

/**
 * Datenspeicher für das Festhalten der Treffer
 * Pro Zielscheibe wird hier ein Zähler festgehalten
 */
uint8_t alteTreffer[TARGET_COUNT];

/**
 * Datenspeicher für die Position der Servos
 * x = -1 -> Servo bleibt unten
 * x = 0  -> Servo nach oben
 * x > 0  -> x ms Zeit bis die Scheibe wieder hochklappen wird
 */
int32_t servoDown[TARGET_SERVOS_COUNT];

/**
 * Datenspeicher für den Timestamp des Spielbeginns
 */
uint32_t startTime;

/**
 * Datenspeicher für Laserzustand
 */
laser_daten_t laserInfo;

/**
 * Datenspeicher für die Aktuell verlaufene Zeit seit Beginn eines Spiels
 */
uint32_t aktZeit;

/**
 * Datenspeicher für den Punktestand
 */
uint32_t score;

/**
 * Datenspeicher für den aktuellen Index der zur Bearbeitung ausgewählten Buchstabe
 */
size_t nameIndex;

/**
 * Datenspeicher für den aktuell ausgewählten Modeindex bei Leaderboardanzeige
 */
size_t curModeLead;

/**
 * Datenspeicher für die aktuelle Position innerhalb des Leaderboards
 */
size_t curRanglistenPos;

/**
 * Datenspeicher für die Leaderboarddaten
 * Realisiert als ein Zeiger auf eine Flashspeicheradresse
 * Wird bei jedem beschreiben des Speichers auf einen aktuell gültigen Zeiger aktualisiert
 */
const ranglisten_arr_t * ranglistenArray; // Nichtflüchtiger Speicher

/**
 * Datenspeicher für die Systemzeit beim letzten Durchlauf des Gameprozesses
 */
uint32_t lastTimeStamp;

/**
 * Datenspeicher für die allgemeine Treffersumme
 */
uint16_t trefferSum;

/**
 * Datenspeicher für das "One up at a time" Verhalten der Scheiben
 * Vermeidet das Wiederhochklappen der gleichen Scheibe
 */
uint8_t oneUpAtATimeLastUp;

bool_t isOnceFired;

#endif
