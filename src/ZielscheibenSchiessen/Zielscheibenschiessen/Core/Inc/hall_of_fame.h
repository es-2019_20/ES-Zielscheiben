/*
 * hall_of_fame.h
 *
 *  Created on: 11.01.2020
 *      Author: Patryk
 */

#ifndef INC_HALL_OF_FAME_H_
#define INC_HALL_OF_FAME_H_
#include "types.h"
#define RANG_SIZE (MODES_CNT * (1 + 1 + GAME_DATEN_HISTORY_LENGTH * (4 + NAME_LENGTH)) + 4)
#define FLASH_SIZE_BYTE 128000
#define FLASH_SIZE_SHORT FLASH_SIZE_BYTE / 2
#define FLASH_SIZE_INT FLASH_SIZE_SHORT / 2
#define FLASH_SIZE_RANG FLASH_SIZE_BYTE / RANG_SIZE
#define FLASH_SIZE_FULL_FLAG FLASH_SIZE_RANG * RANG_SIZE / 8 + (FLASH_SIZE_RANG * RANG_SIZE % 8 == 0 ? 0 : 1)

#if FLASH_SIZE_BYTE - FLASH_SIZE_RANG * RANG_SIZE < FLASH_SIZE_FULL_FLAG
#undef FLASH_SIZE_RANG
#define FLASH_SIZE_RANG FLASH_SIZE_BYTE / RANG_SIZE - 1
#undef FLASH_SIZE_FULL_FLAG
#define FLASH_SIZE_FULL_FLAG FLASH_SIZE_RANG * RANG_SIZE / 8 + (FLASH_SIZE_RANG * RANG_SIZE % 8 == 0 ? 0 : 1)
#endif

/**
 * Initialisiert das Leaderboard
 */
bool_t initHOF();

/**
 * Schreibt neue Daten in das Leaderboard
 */
const ranglisten_arr_t* HOF_writeNewData(const rangliste_daten_t *data,
		const ranglisten_arr_t **ra);

/**
 * Gibt die aktuelle Flashadresse des Leaderboards zurück
 */
const ranglisten_arr_t* HOF_getCurDataPos();

#endif /* INC_HALL_OF_FAME_H_ */
