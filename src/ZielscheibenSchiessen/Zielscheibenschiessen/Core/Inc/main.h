/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define Umgebung2_Pin GPIO_PIN_0
#define Umgebung2_GPIO_Port GPIOC
#define Zielscheibe1_Pin GPIO_PIN_1
#define Zielscheibe1_GPIO_Port GPIOC
#define Zielscheibe2_Pin GPIO_PIN_2
#define Zielscheibe2_GPIO_Port GPIOC
#define Zielscheibe3_Pin GPIO_PIN_3
#define Zielscheibe3_GPIO_Port GPIOC
#define RotaryCLK_Pin GPIO_PIN_0
#define RotaryCLK_GPIO_Port GPIOA
#define RotaryCCLK_Pin GPIO_PIN_1
#define RotaryCCLK_GPIO_Port GPIOA
#define Servo2_Pin GPIO_PIN_6
#define Servo2_GPIO_Port GPIOA
#define Servo3_Pin GPIO_PIN_7
#define Servo3_GPIO_Port GPIOA
#define Zielscheibe4_Pin GPIO_PIN_4
#define Zielscheibe4_GPIO_Port GPIOC
#define Zielscheibe5_Pin GPIO_PIN_5
#define Zielscheibe5_GPIO_Port GPIOC
#define Servo4_Pin GPIO_PIN_0
#define Servo4_GPIO_Port GPIOB
#define Umgebung1_Pin GPIO_PIN_1
#define Umgebung1_GPIO_Port GPIOB
#define RotaryBTN_Pin GPIO_PIN_14
#define RotaryBTN_GPIO_Port GPIOB
#define RotaryBTN_EXTI_IRQn EXTI15_10_IRQn
#define FXDFPlayerBSY_Pin GPIO_PIN_15
#define FXDFPlayerBSY_GPIO_Port GPIOB
#define FXDFPlayerBSY_EXTI_IRQn EXTI15_10_IRQn
#define FXDFPlayerTX_Pin GPIO_PIN_6
#define FXDFPlayerTX_GPIO_Port GPIOC
#define FXDFPlayerRx_Pin GPIO_PIN_7
#define FXDFPlayerRx_GPIO_Port GPIOC
#define Servo5_Pin GPIO_PIN_9
#define Servo5_GPIO_Port GPIOC
#define MusikDFPlayerBSY_Pin GPIO_PIN_8
#define MusikDFPlayerBSY_GPIO_Port GPIOA
#define MusikDFPlayerBSY_EXTI_IRQn EXTI9_5_IRQn
#define MusikDFPlayerTX_Pin GPIO_PIN_9
#define MusikDFPlayerTX_GPIO_Port GPIOA
#define MusikDFPlayerRx_Pin GPIO_PIN_10
#define MusikDFPlayerRx_GPIO_Port GPIOA
#define Laser_Pin GPIO_PIN_10
#define Laser_GPIO_Port GPIOC
#define Abzug_Pin GPIO_PIN_12
#define Abzug_GPIO_Port GPIOC
#define Abzug_EXTI_IRQn EXTI15_10_IRQn
#define Servo1_Pin GPIO_PIN_6
#define Servo1_GPIO_Port GPIOB
#define OLED_I2C_SCL_Pin GPIO_PIN_8
#define OLED_I2C_SCL_GPIO_Port GPIOB
#define OLED_I2C_SDA_Pin GPIO_PIN_9
#define OLED_I2C_SDA_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
