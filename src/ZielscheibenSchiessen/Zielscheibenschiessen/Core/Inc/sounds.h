/*
 * sounds.h
 *
 *  Created on: 13.01.2020
 *      Author: Patryk
 */

#ifndef INC_SOUNDS_H_
#define INC_SOUNDS_H_
#include "stm32f4xx_hal.h"
#include "types.h"

#define MUSIC_DF 0
#define FX_DF 1

#define DFPLAYER_EQ_NORMAL 0
#define DFPLAYER_EQ_POP 1
#define DFPLAYER_EQ_ROCK 2
#define DFPLAYER_EQ_JAZZ 3
#define DFPLAYER_EQ_CLASSIC 4
#define DFPLAYER_EQ_BASS 5

#define DFPLAYER_DEVICE_U_DISK 1
#define DFPLAYER_DEVICE_SD 2
#define DFPLAYER_DEVICE_AUX 3
#define DFPLAYER_DEVICE_SLEEP 4
#define DFPLAYER_DEVICE_FLASH 5

#define DFPLAYER_RECEIVED_LENGTH 10
#define DFPLAYER_SEND_LENGTH 10

//#define _DEBUG

#define TimeOut 0
#define WrongStack 1
#define DFPlayerCardInserted 2
#define DFPlayerCardRemoved 3
#define DFPlayerCardOnline 4
#define DFPlayerPlayFinished 5
#define DFPlayerError 6
#define DFPlayerUSBInserted 7
#define DFPlayerUSBRemoved 8
#define DFPlayerUSBOnline 9
#define DFPlayerCardUSBOnline 10
#define DFPlayerFeedBack 11

#define Busy 1
#define Sleeping 2
#define SerialWrongStack 3
#define CheckSumNotMatch 4
#define FileIndexOut 5
#define FileMismatch 6
#define Advertise 7

#define Stack_Header 0
#define Stack_Version 1
#define Stack_Length 2
#define Stack_Command 3
#define Stack_ACK 4
#define Stack_Parameter 5
#define Stack_CheckSum 7
#define Stack_End 9

//  bool_t handleMessage(uint8_t which, uint8_t type, uint16_t parameter);
//
//  bool_t handleError(uint8_t which, uint8_t type, uint16_t parameter);
//
//  uint8_t readType(uint8_t which);
//
//  uint16_t read(uint8_t which);
//
//  uint8_t readCommand(uint8_t which);

//  bool_t commit(uint8_t which);

//  bool_t waitAvailable(uint8_t which, uint32_t duration);

//  bool_t isAvailable(uint8_t which);

	bool_t isPlaying(uint8_t which);

  void enableACK(uint8_t which);

  void disableACK(uint8_t which);

  /**
   * Initialisiert den DFPlayer
   */
  void* begin(UART_HandleTypeDef *huart, uint8_t which/*, bool_t isACK*/, bool_t doReset);

  //void setTimeOut(uint8_t which, uint32_t timeOutDuration);

  void next(uint8_t which);

  void previous(uint8_t which);

  void play(uint8_t which, int32_t fileNumber);

  void volumeUp(uint8_t which);

  void volumeDown(uint8_t which);

  void volume(uint8_t which, uint8_t volume);

  void EQ(uint8_t which, uint8_t eq);

  void loop(uint8_t which, int32_t fileNumber);

  void outputDevice(uint8_t which, uint8_t device);

  void sleep(uint8_t which);

  void dfplayer_reset(uint8_t which);

  void dfplayer_start(uint8_t which);

  void pause(uint8_t which);

  void playFolder(uint8_t which, uint8_t folderNumber, uint8_t fileNumber);

  void outputSetting(uint8_t which, bool_t enable, uint8_t gain);

  void enableLoopAll(uint8_t which);

  void disableLoopAll(uint8_t which);

  void playMp3Folder(uint8_t which, int32_t fileNumber);

  void advertise(uint8_t which, int32_t fileNumber);

  void playLargeFolder(uint8_t which, uint8_t folderNumber, uint16_t fileNumber);

  void stopAdvertise(uint8_t which);

  void stop(uint8_t which);

  void loopFolder(uint8_t which, int32_t folderNumber);

  void randomAll(uint8_t which);

  void enableLoop(uint8_t which);

  void disableLoop(uint8_t which);

  void enableDAC(uint8_t which);

  void disableDAC(uint8_t which);

//  int readState(uint8_t which);
//
//  int readVolume(uint8_t which);
//
//  int readEQ(uint8_t which);
//
//  int readFileCountsFromDevice(uint8_t which, uint8_t device);
//
//  int readCurrentFileNumberFromDevice(uint8_t which, uint8_t device);
//
//  int readFileCountsInFolder(uint8_t which, int32_t folderNumber);
//
//  int readFileCounts(uint8_t which);
//
//  int readFolderCounts(uint8_t which);
//
//  int readCurrentFileNumber(uint8_t which);

  void notifyFinishedPlaying(void * df_handle);

#endif /* INC_SOUNDS_H_ */
